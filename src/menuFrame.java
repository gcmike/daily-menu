import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.RegionUtil;

import java.util.*;
import java.io.*;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JToggleButton;
import javax.swing.JSlider;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class menuFrame extends JFrame {

	private JPanel contentPane;
	private JTextField address;
	private JTextField addressC;
	private JTextField oName;
	
	ArrayList<patient> today = new ArrayList<patient>();
	ArrayList<dishList> lList = new ArrayList<dishList>();
	ArrayList<dishList> dList = new ArrayList<dishList>();
	ArrayList<dishList> mList = new ArrayList<dishList>();
	ArrayList<prepareItem> pList = new ArrayList<prepareItem>();
	ArrayList<prepareItem> nList = new ArrayList<prepareItem>();
	ArrayList<dish> dishMat = new ArrayList<dish>();
	ArrayList<diffDish> dfList = new ArrayList<diffDish>();
	
	final DefaultListModel<String> listModelL = new DefaultListModel<String>();
	final DefaultListModel<String> listModelD = new DefaultListModel<String>();
	final DefaultListModel<String> listModelM = new DefaultListModel<String>();
	private JTextField newDishName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menuFrame frame = new menuFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					StringWriter er = new StringWriter();
					e.printStackTrace(new PrintWriter(er));
					JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public menuFrame() {
		
		final ArrayList<JComboBox> lTaste = new ArrayList<JComboBox>();
		final ArrayList<JFormattedTextField> lName = new ArrayList<JFormattedTextField>();
		final ArrayList<JToggleButton> lSize = new ArrayList<JToggleButton>();
		final ArrayList<JFormattedTextField> lCmt = new ArrayList<JFormattedTextField>();
		final ArrayList<JComboBox> dTaste = new ArrayList<JComboBox>();
		final ArrayList<JFormattedTextField> dName = new ArrayList<JFormattedTextField>();
		final ArrayList<JToggleButton> dSize = new ArrayList<JToggleButton>();
		final ArrayList<JFormattedTextField> dCmt = new ArrayList<JFormattedTextField>();
		
		setTitle("MENU");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 686, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 660, 384);
		contentPane.add(tabbedPane);
		
		final DefaultListModel<String> listModel = new DefaultListModel<String>();
		final DefaultListModel<String> listModel2 = new DefaultListModel<String>();
		
		JPanel dishOrder = new JPanel();
		tabbedPane.addTab("\u83DC\u55AE\u9806\u5E8F", null, dishOrder, null);
		dishOrder.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 54, 147, 269);
		dishOrder.add(scrollPane_1);
		
		final JList<String> menuListL = new JList<String>(listModelL);
		scrollPane_1.setViewportView(menuListL);
		
		JButton goUpL = new JButton("\u21e7");
		goUpL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idx = menuListL.getSelectedIndex();
				if(idx > 0){
					dishList tmp = lList.get(idx);
					lList.add(idx-1, tmp);
					lList.remove(idx+1);
					sortList();
					menuListL.setSelectedIndex(idx-1);
				}
			}
		});
		goUpL.setBounds(10, 322, 57, 23);
		dishOrder.add(goUpL);
		
		JButton goDownL = new JButton("\u21e9");
		goDownL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListL.getSelectedIndex();
				if(idx < lList.size()-1){
					dishList tmp = lList.get(idx);
					lList.remove(idx);
					lList.add(idx+1,tmp);
					sortList();
					menuListL.setSelectedIndex(idx+1);
				}
			}
		});
		goDownL.setBounds(100, 322, 57, 23);
		dishOrder.add(goDownL);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(498, 54, 147, 269);
		dishOrder.add(scrollPane_2);
		
		final JList<String> menuListD = new JList<String>(listModelD);
		scrollPane_2.setViewportView(menuListD);
		
		JButton goUpD = new JButton("\u21e7");
		goUpD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListD.getSelectedIndex();
				if(idx > 0){
					dishList tmp = dList.get(idx);
					dList.add(idx-1, tmp);
					dList.remove(idx+1);
					sortList();
					menuListD.setSelectedIndex(idx-1);
				}
			}
		});
		goUpD.setBounds(498, 322, 57, 23);
		dishOrder.add(goUpD);
		
		JButton goDownD = new JButton("\u21e9");
		goDownD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListD.getSelectedIndex();
				if(idx < dList.size()-1){
					dishList tmp = dList.get(idx);
					dList.remove(idx);
					dList.add(idx+1, tmp);
					sortList();
					menuListD.setSelectedIndex(idx+1);
				}
			}
		});
		goDownD.setBounds(588, 322, 57, 23);
		dishOrder.add(goDownD);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(254, 54, 147, 269);
		dishOrder.add(scrollPane_3);
		
		final JList<String> menuListM = new JList<String>(listModelM);
		scrollPane_3.setViewportView(menuListM);
		
		JButton goUpM = new JButton("\u21E7");
		goUpM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListM.getSelectedIndex();
				if(idx > 0){
					dishList tmp = mList.get(idx);
					mList.add(idx-1, tmp);
					mList.remove(idx+1);
					sortList();
					menuListM.setSelectedIndex(idx-1);
				}
			}
		});
		goUpM.setBounds(254, 322, 57, 23);
		dishOrder.add(goUpM);
		
		JButton goDownM = new JButton("\u21E9");
		goDownM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListM.getSelectedIndex();
				if(idx < mList.size()-1){
					dishList tmp = mList.get(idx);
					mList.remove(idx);
					mList.add(idx+1, tmp);
					sortList();
					menuListM.setSelectedIndex(idx+1);
				}
			}
		});
		goDownM.setBounds(344, 322, 57, 23);
		dishOrder.add(goDownM);
		
		JLabel lblNewLabel = new JLabel("\u4E2D\u5348\u51FA\u9910");
		lblNewLabel.setFont(new Font("新細明體", Font.PLAIN, 15));
		lblNewLabel.setBounds(48, 10, 65, 23);
		dishOrder.add(lblNewLabel);
		
		JLabel label = new JLabel("\u665A\u4E0A\u51FA\u9910");
		label.setFont(new Font("新細明體", Font.PLAIN, 15));
		label.setBounds(539, 10, 65, 23);
		dishOrder.add(label);
		
		JButton LtoM = new JButton("\u21d2");
		LtoM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idx = menuListL.getSelectedIndices()[menuListL.getSelectedIndices().length-1];
				for(int id : menuListL.getSelectedIndices())
					if(id >= 0)
						mList.add(lList.get(id));
				int[] tmp = menuListL.getSelectedIndices();
				int tmpSize = tmp.length;
				for(int i = tmp.length-1; i >= 0; i--)
					lList.remove(tmp[i]);
				sortList();
				if(lList.size()>0){
					if(idx < lList.size()-1)
						menuListL.setSelectedIndex(tmp[tmpSize-1]-tmpSize+1);
					else
						menuListL.setSelectedIndex(lList.size()-1);
				}
			}
		});
		LtoM.setBounds(167, 107, 57, 86);
		dishOrder.add(LtoM);
		
		JButton MtoL = new JButton("\u21da");
		MtoL.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				while(mList.size()>0){
					lList.add(mList.get(0));
					mList.remove(0);
				}
				sortList();
			}
		});
		MtoL.setBounds(187, 52, 57, 23);
		dishOrder.add(MtoL);
		
		JButton DtoM = new JButton("\u21d0");
		DtoM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = menuListD.getSelectedIndices()[menuListD.getSelectedIndices().length-1];
				for(int id : menuListD.getSelectedIndices())
					if(id>=0)
						mList.add(dList.get(id));
				int [] tmp = menuListD.getSelectedIndices();
				int tmpSize = tmp.length;
				for(int i = tmp.length-1; i >= 0; i--)
					dList.remove(tmp[i]);
				sortList();
				if(dList.size()>0){
					if(idx < dList.size()-1)
						menuListD.setSelectedIndex(tmp[tmpSize-1]-tmpSize+1);
					else
						menuListD.setSelectedIndex(dList.size()-1);
				}
			}
		});
		DtoM.setBounds(431, 107, 57, 86);
		dishOrder.add(DtoM);
		
		JButton MtoD = new JButton("\u21db");
		MtoD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				while(mList.size()>0){
					dList.add(mList.get(0));
					mList.remove(0);
				}
				sortList();
			}
		});
		MtoD.setBounds(411, 52, 57, 23);
		dishOrder.add(MtoD);
		
		JLabel label_12 = new JLabel("\u6392\u5217\u9806\u5E8F");
		label_12.setFont(new Font("新細明體", Font.PLAIN, 15));
		label_12.setBounds(295, 10, 65, 23);
		dishOrder.add(label_12);
		
		JLabel label_13 = new JLabel("\u8ACB\u5C07\u958B\u80C3\u8543\u8304\u7F6E\u9802");
		label_13.setFont(new Font("新細明體", Font.PLAIN, 12));
		label_13.setBounds(35, 32, 116, 23);
		dishOrder.add(label_13);
		
		JLabel label_14 = new JLabel("\u8ACB\u5C07\u958B\u80C3\u8543\u8304\u7F6E\u9802");
		label_14.setFont(new Font("新細明體", Font.PLAIN, 12));
		label_14.setBounds(523, 32, 116, 23);
		dishOrder.add(label_14);
		
		JPanel personal = new JPanel();
		tabbedPane.addTab("\u500B\u4EBA\u83DC\u55AE", null, personal, null);
		personal.setLayout(null);
		
		final JFormattedTextField pName = new JFormattedTextField();
		pName.setHorizontalAlignment(SwingConstants.LEFT);
		pName.setBounds(10, 41, 96, 21);
		personal.add(pName);
		
		final JToggleButton tomato = new JToggleButton("\u958B\u80C3\u8543\u8304");
		tomato.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				tomato.setText(tomato.isSelected()?"開胃蕃茄":"不蕃茄");
			}
		});
		tomato.setSelected(true);
		tomato.setBounds(10, 72, 96, 20);
		personal.add(tomato);
		
		JComboBox lTaste1 = new JComboBox();
		lTaste1.setBounds(10, 161, 67, 20);
		personal.add(lTaste1);
		lTaste.add(lTaste1);
		
		JFormattedTextField lName1 = new JFormattedTextField();
		lName1.setHorizontalAlignment(SwingConstants.LEFT);
		lName1.setBounds(87, 159, 96, 23);
		personal.add(lName1);
		lName.add(lName1);
		
		JFormattedTextField lCmt1 = new JFormattedTextField();
		lCmt1.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt1.setBounds(243, 159, 70, 21);
		personal.add(lCmt1);
		lCmt.add(lCmt1);
		
		final JToggleButton lSize1 = new JToggleButton("\u534A");
		lSize1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				lSize1.setText(lSize1.isSelected()?"全":"半");
			}
		});
		lSize1.setBounds(188, 159, 50, 23);
		personal.add(lSize1);
		lSize.add(lSize1);
		
		JComboBox lTaste2 = new JComboBox();
		lTaste2.setBounds(10, 194, 67, 21);
		personal.add(lTaste2);
		lTaste.add(lTaste2);
		
		JFormattedTextField lName2 = new JFormattedTextField();
		lName2.setHorizontalAlignment(SwingConstants.LEFT);
		lName2.setBounds(87, 192, 96, 23);
		personal.add(lName2);
		lName.add(lName2);
		
		final JToggleButton lSize2 = new JToggleButton("\u534A");
		lSize2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lSize2.setText(lSize2.isSelected()?"全":"半");
			}
		});
		lSize2.setBounds(188, 192, 50, 23);
		personal.add(lSize2);
		lSize.add(lSize2);
		
		JFormattedTextField lCmt2 = new JFormattedTextField();
		lCmt2.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt2.setBounds(243, 192, 70, 21);
		personal.add(lCmt2);
		lCmt.add(lCmt2);
		
		JComboBox lTaste3 = new JComboBox();
		lTaste3.setBounds(10, 227, 67, 21);
		personal.add(lTaste3);
		lTaste.add(lTaste3);
		
		JFormattedTextField lName3 = new JFormattedTextField();
		lName3.setHorizontalAlignment(SwingConstants.LEFT);
		lName3.setBounds(87, 225, 96, 23);
		personal.add(lName3);
		lName.add(lName3);
		
		final JToggleButton lSize3 = new JToggleButton("\u534A");
		lSize3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lSize3.setText(lSize3.isSelected()?"全":"半");
			}
		});
		lSize3.setBounds(188, 225, 50, 23);
		personal.add(lSize3);
		lSize.add(lSize3);
		
		JFormattedTextField lCmt3 = new JFormattedTextField();
		lCmt3.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt3.setBounds(243, 225, 70, 21);
		personal.add(lCmt3);
		lCmt.add(lCmt3);
		
		JComboBox lTaste4 = new JComboBox();
		lTaste4.setBounds(10, 260, 67, 21);
		personal.add(lTaste4);
		lTaste.add(lTaste4);
		
		JComboBox lTaste5 = new JComboBox();
		lTaste5.setBounds(10, 293, 67, 21);
		personal.add(lTaste5);
		lTaste.add(lTaste5);
		
		JComboBox lTaste6 = new JComboBox();
		lTaste6.setBounds(10, 326, 67, 21);
		personal.add(lTaste6);
		lTaste.add(lTaste6);
		
		JFormattedTextField lName4 = new JFormattedTextField();
		lName4.setHorizontalAlignment(SwingConstants.LEFT);
		lName4.setBounds(87, 258, 96, 23);
		personal.add(lName4);
		lName.add(lName4);
		
		JFormattedTextField lName5 = new JFormattedTextField();
		lName5.setHorizontalAlignment(SwingConstants.LEFT);
		lName5.setBounds(87, 291, 96, 23);
		personal.add(lName5);
		lName.add(lName5);
		
		JFormattedTextField lName6 = new JFormattedTextField();
		lName6.setHorizontalAlignment(SwingConstants.LEFT);
		lName6.setBounds(87, 324, 96, 23);
		personal.add(lName6);
		lName.add(lName6);
		
		final JToggleButton lSize4 = new JToggleButton("\u534A");
		lSize4.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lSize4.setText(lSize4.isSelected()?"全":"半");
			}
		});
		lSize4.setBounds(188, 258, 50, 23);
		personal.add(lSize4);
		lSize.add(lSize4);
		
		final JToggleButton lSize5 = new JToggleButton("\u534A");
		lSize5.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lSize5.setText(lSize5.isSelected()?"全":"半");
			}
		});
		lSize5.setBounds(188, 291, 50, 23);
		personal.add(lSize5);
		lSize.add(lSize5);
		
		final JToggleButton lSize6 = new JToggleButton("\u534A");
		lSize6.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				lSize6.setText(lSize6.isSelected()?"全":"半");
			}
		});
		lSize6.setBounds(188, 324, 50, 23);
		personal.add(lSize6);
		lSize.add(lSize6);
		
		JFormattedTextField lCmt4 = new JFormattedTextField();
		lCmt4.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt4.setBounds(243, 258, 70, 21);
		personal.add(lCmt4);
		lCmt.add(lCmt4);
		
		JFormattedTextField lCmt5 = new JFormattedTextField();
		lCmt5.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt5.setBounds(243, 291, 70, 21);
		personal.add(lCmt5);
		lCmt.add(lCmt5);
		
		JFormattedTextField lCmt6 = new JFormattedTextField();
		lCmt6.setHorizontalAlignment(SwingConstants.LEFT);
		lCmt6.setBounds(243, 324, 70, 21);
		personal.add(lCmt6);
		lCmt.add(lCmt6);
		
		JComboBox dTaste1 = new JComboBox();
		dTaste1.setBounds(342, 161, 67, 21);
		personal.add(dTaste1);
		dTaste.add(dTaste1);
		
		JComboBox dTaste2 = new JComboBox();
		dTaste2.setBounds(342, 194, 67, 21);
		personal.add(dTaste2);
		dTaste.add(dTaste2);
		
		JComboBox dTaste3 = new JComboBox();
		dTaste3.setBounds(342, 227, 67, 21);
		personal.add(dTaste3);
		dTaste.add(dTaste3);
		
		JFormattedTextField dName1 = new JFormattedTextField();
		dName1.setHorizontalAlignment(SwingConstants.LEFT);
		dName1.setBounds(419, 159, 96, 23);
		personal.add(dName1);
		dName.add(dName1);
		
		JFormattedTextField dName2 = new JFormattedTextField();
		dName2.setHorizontalAlignment(SwingConstants.LEFT);
		dName2.setBounds(419, 192, 96, 23);
		personal.add(dName2);
		dName.add(dName2);
		
		JFormattedTextField dName3 = new JFormattedTextField();
		dName3.setHorizontalAlignment(SwingConstants.LEFT);
		dName3.setBounds(419, 225, 96, 23);
		personal.add(dName3);
		dName.add(dName3);
		
		final JToggleButton dSize1 = new JToggleButton("\u534A");
		dSize1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize1.setText(dSize1.isSelected()?"全":"半");
			}
		});
		dSize1.setBounds(520, 159, 50, 23);
		personal.add(dSize1);
		dSize.add(dSize1);
		
		final JToggleButton dSize2 = new JToggleButton("\u534A");
		dSize2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize2.setText(dSize2.isSelected()?"全":"半");
			}
		});
		dSize2.setBounds(520, 192, 50, 23);
		personal.add(dSize2);
		dSize.add(dSize2);
		
		final JToggleButton dSize3 = new JToggleButton("\u534A");
		dSize3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize3.setText(dSize3.isSelected()?"全":"半");
			}
		});
		dSize3.setBounds(520, 225, 50, 23);
		personal.add(dSize3);
		dSize.add(dSize3);
		
		JFormattedTextField dCmt1 = new JFormattedTextField();
		dCmt1.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt1.setBounds(575, 159, 70, 21);
		personal.add(dCmt1);
		dCmt.add(dCmt1);
		
		JFormattedTextField dCmt2 = new JFormattedTextField();
		dCmt2.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt2.setBounds(575, 192, 70, 21);
		personal.add(dCmt2);
		dCmt.add(dCmt2);
		
		JFormattedTextField dCmt3 = new JFormattedTextField();
		dCmt3.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt3.setBounds(575, 225, 70, 21);
		personal.add(dCmt3);
		dCmt.add(dCmt3);
		
		JComboBox dTaste4 = new JComboBox();
		dTaste4.setBounds(342, 260, 67, 21);
		personal.add(dTaste4);
		dTaste.add(dTaste4);
		
		JComboBox dTaste5 = new JComboBox();
		dTaste5.setBounds(342, 293, 67, 21);
		personal.add(dTaste5);
		dTaste.add(dTaste5);
		
		JComboBox dTaste6 = new JComboBox();
		dTaste6.setBounds(342, 326, 67, 21);
		personal.add(dTaste6);
		dTaste.add(dTaste6);
		
		JFormattedTextField dName4 = new JFormattedTextField();
		dName4.setHorizontalAlignment(SwingConstants.LEFT);
		dName4.setBounds(419, 258, 96, 23);
		personal.add(dName4);
		dName.add(dName4);
		
		JFormattedTextField dName5 = new JFormattedTextField();
		dName5.setHorizontalAlignment(SwingConstants.LEFT);
		dName5.setBounds(419, 291, 96, 23);
		personal.add(dName5);
		dName.add(dName5);
		
		JFormattedTextField dName6 = new JFormattedTextField();
		dName6.setHorizontalAlignment(SwingConstants.LEFT);
		dName6.setBounds(419, 324, 96, 23);
		personal.add(dName6);
		dName.add(dName6);
		
		final JToggleButton dSize4 = new JToggleButton("\u534A");
		dSize4.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize4.setText(dSize4.isSelected()?"全":"半");
			}
		});
		dSize4.setBounds(520, 258, 50, 23);
		personal.add(dSize4);
		dSize.add(dSize4);
		
		final JToggleButton dSize5 = new JToggleButton("\u534A");
		dSize5.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize5.setText(dSize5.isSelected()?"全":"半");
			}
		});
		dSize5.setBounds(520, 291, 50, 23);
		personal.add(dSize5);
		dSize.add(dSize5);
		
		final JToggleButton dSize6 = new JToggleButton("\u534A");
		dSize6.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				dSize6.setText(dSize6.isSelected()?"全":"半");
			}
		});
		dSize6.setBounds(520, 324, 50, 23);
		personal.add(dSize6);
		dSize.add(dSize6);
		
		JFormattedTextField dCmt4 = new JFormattedTextField();
		dCmt4.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt4.setBounds(575, 258, 70, 21);
		personal.add(dCmt4);
		dCmt.add(dCmt4);
		
		JFormattedTextField dCmt5 = new JFormattedTextField();
		dCmt5.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt5.setBounds(575, 291, 70, 21);
		personal.add(dCmt5);
		dCmt.add(dCmt5);
		
		JFormattedTextField dCmt6 = new JFormattedTextField();
		dCmt6.setHorizontalAlignment(SwingConstants.LEFT);
		dCmt6.setBounds(575, 324, 70, 21);
		personal.add(dCmt6);
		dCmt.add(dCmt6);
		
		final JComboBox bTaste = new JComboBox();
		bTaste.setBounds(267, 12, 67, 21);
		personal.add(bTaste);
		
		final JComboBox sTaste = new JComboBox();
		sTaste.setBounds(267, 45, 67, 21);
		personal.add(sTaste);
		
		final JComboBox tTaste = new JComboBox();
		tTaste.setBounds(267, 78, 67, 21);
		personal.add(tTaste);
		
		final JFormattedTextField bName = new JFormattedTextField();
		bName.setHorizontalAlignment(SwingConstants.LEFT);
		bName.setBounds(344, 10, 96, 23);
		personal.add(bName);
		
		final JFormattedTextField sName = new JFormattedTextField();
		sName.setHorizontalAlignment(SwingConstants.LEFT);
		sName.setBounds(344, 43, 96, 23);
		personal.add(sName);
		
		final JFormattedTextField tName = new JFormattedTextField();
		tName.setHorizontalAlignment(SwingConstants.LEFT);
		tName.setBounds(344, 76, 96, 23);
		personal.add(tName);
		
		final JToggleButton bSize = new JToggleButton("\u534A");
		bSize.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				bSize.setText(bSize.isSelected()?"全":"半");
			}
		});
		bSize.setBounds(445, 10, 50, 23);
		personal.add(bSize);
		
		final JToggleButton sSize = new JToggleButton("\u534A");
		sSize.setEnabled(false);
		sSize.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				sSize.setText(sSize.isSelected()?"全":"半");
			}
		});
		sSize.setBounds(445, 43, 50, 23);
		personal.add(sSize);
		
		final JToggleButton tSize = new JToggleButton("\u534A");
		tSize.setEnabled(false);
		tSize.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				tSize.setText(tSize.isSelected()?"全":"半");
			}
		});
		tSize.setBounds(445, 76, 50, 23);
		personal.add(tSize);
		
		final JFormattedTextField bCmt = new JFormattedTextField();
		bCmt.setHorizontalAlignment(SwingConstants.LEFT);
		bCmt.setBounds(500, 10, 70, 21);
		personal.add(bCmt);
		
		final JFormattedTextField sCmt = new JFormattedTextField();
		sCmt.setHorizontalAlignment(SwingConstants.LEFT);
		sCmt.setBounds(500, 43, 70, 21);
		personal.add(sCmt);
		
		final JFormattedTextField tCmt = new JFormattedTextField();
		tCmt.setHorizontalAlignment(SwingConstants.LEFT);
		tCmt.setBounds(500, 76, 70, 21);
		personal.add(tCmt);
		
		final JFormattedTextField toCmt = new JFormattedTextField("");
		toCmt.setHorizontalAlignment(SwingConstants.LEFT);
		toCmt.setBounds(116, 72, 70, 21);
		personal.add(toCmt);
		
		final JSlider shipment = new JSlider();
		shipment.setValue(0);
		shipment.setMaximum(1);
		shipment.setBounds(136, 41, 43, 21);
		personal.add(shipment);
		
		final JSlider order = new JSlider();
		order.setValue(1);
		order.setMaximum(1);
		order.setBounds(136, 10, 43, 21);
		personal.add(order);
		
		final JToggleButton lSAll = new JToggleButton("\u5168\u9078");
		lSAll.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				for(int i = 0; i < lSize.size(); i++)
					lSize.get(i).setSelected(lSAll.isSelected());
			}
		});
		lSAll.setBounds(181, 126, 60, 23);
		personal.add(lSAll);
		
		final JToggleButton dSAll = new JToggleButton("\u5168\u9078");
		dSAll.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				for(int i = 0; i < dSize.size(); i++)
					dSize.get(i).setSelected(dSAll.isSelected());
			}
		});
		dSAll.setBounds(515, 126, 60, 23);
		personal.add(dSAll);
		
		final JComboBox lTAll = new JComboBox();
		lTAll.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				for(int i = 0; i < 6; i++)
					lTaste.get(i).setSelectedIndex(lTAll.getSelectedIndex());
			}
		});
		lTAll.setBounds(10, 126, 67, 20);
		personal.add(lTAll);
		
		final JComboBox dTAll = new JComboBox();
		dTAll.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				for(int i = 0; i < 6; i++)
					dTaste.get(i).setSelectedIndex(dTAll.getSelectedIndex());
			}
		});
		dTAll.setBounds(342, 126, 67, 21);
		personal.add(dTAll);
		
		
		for(int i = 0; i < 6; i++){
			lTaste.get(i).addItem("不調味");
			lTaste.get(i).addItem("淡淡");
			lTaste.get(i).addItem("稍淡");
			lTaste.get(i).addItem("正常");
			lTaste.get(i).addItem("稍重");
			lTaste.get(i).addItem("重");
			dTaste.get(i).addItem("不調味");
			dTaste.get(i).addItem("淡淡");
			dTaste.get(i).addItem("稍淡");
			dTaste.get(i).addItem("正常");
			dTaste.get(i).addItem("稍重");
			dTaste.get(i).addItem("重");
			lTaste.get(i).setSelectedIndex(0);
			dTaste.get(i).setSelectedIndex(0);
		}
		bTaste.addItem("不調味");
		bTaste.addItem("淡淡");
		bTaste.addItem("稍淡");
		bTaste.addItem("正常");
		bTaste.addItem("稍重");
		bTaste.addItem("重");
		bTaste.setSelectedIndex(0);
		sTaste.addItem("無糖");
		sTaste.addItem("少糖");
		sTaste.addItem("正常");
		sTaste.addItem("多糖");
		sTaste.setSelectedIndex(0);
		tTaste.addItem("無糖");
		tTaste.addItem("少糖");
		tTaste.addItem("正常");
		tTaste.setSelectedIndex(0);
		lTAll.addItem("不調味");
		lTAll.addItem("淡淡");
		lTAll.addItem("稍淡");
		lTAll.addItem("正常");
		lTAll.addItem("稍重");
		lTAll.addItem("重");
		lTAll.setSelectedIndex(0);
		dTAll.addItem("不調味");
		dTAll.addItem("淡淡");
		dTAll.addItem("稍淡");
		dTAll.addItem("正常");
		dTAll.addItem("稍重");
		dTAll.addItem("重");
		dTAll.setSelectedIndex(0);
		
		
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"\u8ACB\u9078\u64C7"}));
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				pName.setText("");
				shipment.setValue(0);
				order.setValue(1);
				tomato.setSelected(true);
				toCmt.setText("");
				lSAll.setSelected(false);
				dSAll.setSelected(false);
				lTAll.setSelectedIndex(0);
				dTAll.setSelectedIndex(0);
				for(int i = 0; i < 6; i++){
					lTaste.get(i).setSelectedIndex(0);
					dTaste.get(i).setSelectedIndex(0);
					lName.get(i).setText("");
					dName.get(i).setText("");
					lSize.get(i).setSelected(false);
					dSize.get(i).setSelected(false);
					lCmt.get(i).setText("");
					dCmt.get(i).setText("");
				}
				bTaste.setSelectedIndex(0);
				sTaste.setSelectedIndex(0);
				tTaste.setSelectedIndex(0);
				bName.setText("");
				sName.setText("");
				tName.setText("");
				bSize.setSelected(false);
				sSize.setSelected(false);
				tSize.setSelected(false);
				bCmt.setText("");
				sCmt.setText("");
				tCmt.setText("");
				
				if(comboBox.getSelectedIndex() > 0){
					int idx = comboBox.getSelectedIndex()-1;
					pName.setText(today.get(idx).getName());
					tomato.setSelected(today.get(idx).getTomato());
					toCmt.setText(today.get(idx).getTomatoCmt());
					shipment.setValue(today.get(idx).getShip()? 1 : 0);
					order.setValue(today.get(idx).getOrder()? 1 : 0);
					if(today.get(idx).emptyBkfast()){
						bTaste.setSelectedIndex(today.get(idx).getBkfast(0).getTas());
						bName.setText(today.get(idx).getBkfast(0).getDishName());
						bSize.setSelected(today.get(idx).getBkfast(0).getSize());
						bCmt.setText(today.get(idx).getBkfast(0).getComment());
					}
					if(today.get(idx).emptySweet()){
						sTaste.setSelectedIndex(Math.max(today.get(idx).getSweet(0).getTas()-6,0));
						sName.setText(today.get(idx).getSweet(0).getDishName());
						sSize.setSelected(today.get(idx).getSweet(0).getSize());
						sCmt.setText(today.get(idx).getSweet(0).getComment());
					}
					if(today.get(idx).emptyDrink()){
						tTaste.setSelectedIndex(Math.max(today.get(idx).getDrink(0).getTas()-6,0));
						tName.setText(today.get(idx).getDrink(0).getDishName());
						tSize.setSelected(today.get(idx).getDrink(0).getSize());
						tCmt.setText(today.get(idx).getDrink(0).getComment());
					}
					if(today.get(idx).emptyLunch()){
						for(int i = 0; i < today.get(idx).getLunchCount(); i++){
							lTaste.get(i).setSelectedIndex(today.get(idx).getLunch(i).getTas());
							lName.get(i).setText(today.get(idx).getLunch(i).getDishName());
							lSize.get(i).setSelected(today.get(idx).getLunch(i).getSize());
							lCmt.get(i).setText(today.get(idx).getLunch(i).getComment());
						}
					}
					if(today.get(idx).emptyDinner()){
						for(int i = 0; i < today.get(idx).getDinnerCount(); i++){
							dTaste.get(i).setSelectedIndex(today.get(idx).getDinner(i).getTas());
							dName.get(i).setText(today.get(idx).getDinner(i).getDishName());
							dSize.get(i).setSelected(today.get(idx).getDinner(i).getSize());
							dCmt.get(i).setText(today.get(idx).getDinner(i).getComment());
						}
					}
				}
				
				
				
				
				
				
				
				
				
				
				
				
				
			}
		});
		comboBox.setBounds(10, 10, 96, 21);
		personal.add(comboBox);
		comboBox.addItem("");
		
		JButton delete = new JButton("\u522A\u9664");
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int idx = comboBox.getSelectedIndex()-1;
				delList(today.get(idx));
				today.remove(idx);
				loadCombo(comboBox);
			}
		});
		delete.setBounds(575, 11, 70, 23);
		personal.add(delete);
		
		JButton add = new JButton("\u65B0\u589E");
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				patient tmp = new patient(pName.getText());
				tmp.setShip(shipment.getValue()>0);
				tmp.setOrder(order.getValue()>0);
				tmp.setTomato(tomato.isSelected());
				checkDish("開胃蕃茄","","");
				tmp.setTomatoCmt(toCmt.getText());
				if(!bName.getText().equals("")){
					tmp.addBkfast(new dish(bName.getText(),bTaste.getSelectedIndex(),bCmt.getText(),bSize.isSelected()));
					checkDish(bName.getText(),"","");
				}
				if(!sName.getText().equals("")){
					tmp.addSweet(new dish(sName.getText(),sTaste.getSelectedIndex()+6,sCmt.getText(),sSize.isSelected()));
					checkDish(sName.getText(),"","");
				}
				if(!tName.getText().equals("")){
					tmp.addDrink(new dish(tName.getText(),tTaste.getSelectedIndex()+6,tCmt.getText(),tSize.isSelected()));
					checkDish(tName.getText(),"","");
				}
				for(int i = 0; i < 6; i++)
					if(!lName.get(i).getText().equals("")){
						tmp.addLunch(new dish(lName.get(i).getText(),lTaste.get(i).getSelectedIndex()
								,lCmt.get(i).getText(),lSize.get(i).isSelected()));
						checkDish(lName.get(i).getText(),"","");
					}
				for(int i = 0; i < 6; i++)
					if(!dName.get(i).getText().equals("")){
						tmp.addDinner(new dish(dName.get(i).getText(),dTaste.get(i).getSelectedIndex()
								,dCmt.get(i).getText(),dSize.get(i).isSelected()));
						checkDish(dName.get(i).getText(),"","");
					}
				today.add(tmp);
				checkList(tmp);
				
				pName.setText("");
				shipment.setValue(0);
				order.setValue(1);
				tomato.setSelected(true);
				toCmt.setText("");
				lSAll.setSelected(false);
				dSAll.setSelected(false);
				lTAll.setSelectedIndex(3);
				dTAll.setSelectedIndex(3);
				for(int i = 0; i < 6; i++){
					lTaste.get(i).setSelectedIndex(3);
					dTaste.get(i).setSelectedIndex(3);
					lName.get(i).setText("");
					dName.get(i).setText("");
					lSize.get(i).setSelected(false);
					dSize.get(i).setSelected(false);
					lCmt.get(i).setText("");
					dCmt.get(i).setText("");
				}
				bTaste.setSelectedIndex(3);
				sTaste.setSelectedIndex(2);
				tTaste.setSelectedIndex(2);
				bName.setText("");
				sName.setText("");
				tName.setText("");
				bSize.setSelected(false);
				sSize.setSelected(false);
				tSize.setSelected(false);
				bCmt.setText("");
				sCmt.setText("");
				tCmt.setText("");
				
				loadCombo(comboBox);
			}
		});
		add.setBounds(575, 42, 70, 23);
		personal.add(add);
		
		JButton go = new JButton("\u66F4\u65B0");
		go.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] rice = {"白軟飯","白軟","糙米","糙米飯","五榖","五榖飯","五穀","五穀飯","白飯","糙米軟","五榖軟","五榖軟爛","五穀軟","五穀軟爛"};
				
				for(int i = 0; i < lName.size(); i++)
					for(int j = 1; j < 8; j+=2)
						if(lName.get(i).getText().equals(rice[j]))
							lName.get(i).setText(rice[j-1]);
				for(int i = 0; i < dName.size(); i++)
					for(int j = 1; j < 8; j+=2)
						if(dName.get(i).getText().equals(rice[j]))
							dName.get(i).setText(rice[j-1]);
				for(int i = 0; i < lName.size(); i++)
					for(int j = 0; j < 14; j++)
						if(lName.get(i).getText().equals(rice[j]))
							lTaste.get(i).setSelectedIndex(3);
				for(int i = 0; i < dName.size(); i++)
					for(int j = 0; j < 14; j++)
						if(dName.get(i).getText().equals(rice[j]))
							dTaste.get(i).setSelectedIndex(3);
				
				int idx = comboBox.getSelectedIndex()-1;
				
				today.get(idx).setName(pName.getText());
				today.get(idx).setShip(shipment.getValue()>0);
				today.get(idx).setOrder(order.getValue()>0);
				today.get(idx).setTomato(tomato.isSelected());
				today.get(idx).setTomatoCmt(toCmt.getText());
				
				today.get(idx).clearBkfast();
				if(!bName.getText().equals(""))
					today.get(idx).addBkfast(new dish(bName.getText(),bTaste.getSelectedIndex(),bCmt.getText(),bSize.isSelected()));
				
				today.get(idx).clearSweet();
				if(!sName.getText().equals(""))
					today.get(idx).addSweet(new dish(sName.getText(),sTaste.getSelectedIndex()+6,sCmt.getText(),sSize.isSelected()));
					
				today.get(idx).clearDrink();
				if(!tName.getText().equals(""))
					today.get(idx).addDrink(new dish(tName.getText(),tTaste.getSelectedIndex()+6,tCmt.getText(),tSize.isSelected()));
				
				today.get(idx).clearLunch();
				for(int i = 0 ; i < 6; i ++){
					if(!lName.get(i).getText().equals(""))
						today.get(idx).addLunch(new dish(lName.get(i).getText(),lTaste.get(i).getSelectedIndex()
								,lCmt.get(i).getText(),lSize.get(i).isSelected()));
				}
				today.get(idx).clearDinner();
				for(int i = 0 ; i < 6; i ++){
					if(!dName.get(i).getText().equals(""))
						today.get(idx).addDinner(new dish(dName.get(i).getText(),dTaste.get(i).getSelectedIndex()
								,dCmt.get(i).getText(),dSize.get(i).isSelected()));
				}
				
				delList(today.get(idx));
				
				if(order.getValue() != 0){
					checkList(today.get(idx));
				}
				
				/*patient tmp = new patient(pName.getText());
				tmp.setShip(shipment.getValue()>0);
				tmp.setTomato(tomato.isSelected());
				today.add(tmp);*/
				
				//comboBox.addItem(pName.getText());
				pName.setText("");
				shipment.setValue(0);
				order.setValue(1);
				tomato.setSelected(true);
				toCmt.setText("");
				lSAll.setSelected(false);
				dSAll.setSelected(false);
				lTAll.setSelectedIndex(0);
				dTAll.setSelectedIndex(0);
				for(int i = 0; i < 6; i++){
					lTaste.get(i).setSelectedIndex(0);
					dTaste.get(i).setSelectedIndex(0);
					lName.get(i).setText("");
					dName.get(i).setText("");
					lSize.get(i).setSelected(false);
					dSize.get(i).setSelected(false);
					lCmt.get(i).setText("");
					dCmt.get(i).setText("");
				}
				bTaste.setSelectedIndex(0);
				sTaste.setSelectedIndex(0);
				tTaste.setSelectedIndex(0);
				bName.setText("");
				sName.setText("");
				tName.setText("");
				bSize.setSelected(false);
				sSize.setSelected(false);
				tSize.setSelected(false);
				bCmt.setText("");
				sCmt.setText("");
				tCmt.setText("");
				
				loadCombo(comboBox);
				if(idx+1 == today.size())
					JOptionPane.showMessageDialog(null, "沒人了", "End of List", JOptionPane.WARNING_MESSAGE);
				else
					comboBox.setSelectedIndex(idx+2);
			}
		});
		go.setBounds(575, 72, 70, 44);
		personal.add(go);
		
		JPanel imExport = new JPanel();
		tabbedPane.addTab("\u532F\u5165 / \u532F\u51FA\u8868\u55AE", null, imExport, null);
		imExport.setLayout(null);
		tabbedPane.setSelectedIndex(2);
		
		address = new JTextField();
		address.setEditable(false);
		address.setBounds(10, 10, 294, 21);
		imExport.add(address);
		address.setColumns(10);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 59, 185, 286);
		imExport.add(scrollPane);
		final JList<String> list = new JList<String>(listModel);
		scrollPane.setViewportView(list);
		
		oName = new JTextField();
		oName.setText("Output");
		oName.setBounds(384, 272, 96, 21);
		imExport.add(oName);
		oName.setColumns(10);
		
		JButton btnOpen = new JButton("\u958B\u555F");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listModel.removeAllElements();
				JFileChooser fch = new JFileChooser(System.getProperty("user.dir"));
				FileNameExtensionFilter fil = new FileNameExtensionFilter("XLS* files","xlsx","xls");
				fch.setFileFilter(fil);
				int returnVal = fch.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					address.setText(fch.getSelectedFile().getAbsolutePath());
					try{
						InputStream inp = new FileInputStream(fch.getSelectedFile().getAbsolutePath());
						XSSFWorkbook wb = new XSSFWorkbook(inp);
						/*Sheet sht = wb.getSheetAt(7);
						//for(int i = 0; i < sht.getPhysicalNumberOfRows(); i++){
						//Row rw = sht.getRow(i);
						Row rw = sht.getRow(18);
						Cell cll = rw.getCell(0);
						//System.out.println(cll.toString());
						textField_1.setText(cll.toString());
						//}*/
						
						for(int i = 0; i < wb.getNumberOfSheets(); i++)
							listModel.addElement(wb.getSheetName(i));
						
						inp.close();
					}catch(IOException e){
						StringWriter er = new StringWriter();
						e.printStackTrace(new PrintWriter(er));
						JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
					}
					// textField_1.setText("You select: " + fch.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnOpen.setBounds(217, 57, 87, 23);
		imExport.add(btnOpen);
		
		JButton btnImport = new JButton("\u532F\u5165");
		btnImport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!list.isSelectionEmpty())
					try{
						InputStream inp = new FileInputStream(address.getText());
						XSSFWorkbook wb = new XSSFWorkbook(inp);
						for(int i : list.getSelectedIndices()){
							Sheet sht = wb.getSheetAt(i);
							
							patient tmp = new patient(sht.getSheetName());
							Row rw = sht.getRow(2); //蕃茄
							Cell cll = rw.getCell(2);
							Cell cl1 = rw.getCell(3);
							Cell cl2 = rw.getCell(4);
							if(cll.toString().equals("不蕃茄") || cll.toString().equals("不番茄"))
								tmp.setTomato(false);
							else{
								tmp.setTomato(true);
								tmp.setTomatoCmt(cll.toString());
							}
							rw = sht.getRow(6); // 養生茶飲
							cll = rw.getCell(2);
							cl1 = rw.getCell(3);
							cl2 = rw.getCell(4);
							if(cll != null && !cll.toString().equals("")){
								tmp.addDrink(new dish(cll.toString()));
								checkDish(cll.toString(), cl1.toString(), cl2.toString());
							}
							rw = sht.getRow(14); // 甜點
							cll = rw.getCell(2);
							cl1 = rw.getCell(3);
							cl2 = rw.getCell(4);
							if(cll != null && !cll.toString().equals("")){
								tmp.addSweet(new dish(cll.toString()));
								checkDish(cll.toString(), cl1.toString(), cl2.toString());
							}
							
							rw = sht.getRow(10); // Breakfast
							cll = rw.getCell(2);
							cl1 = rw.getCell(3);
							cl2 = rw.getCell(4);
							if(cll != null && !cll.toString().equals("")){
								tmp.addBkfast(new dish(cll.toString()));
								checkDish(cll.toString(), cl1.toString(), cl2.toString());
							}
							
							int din; // get Dinner row number
							for(din = 22; din < 50; din+=4){
								rw = sht.getRow(din);
								cll = rw.getCell(0);
								if(!cll.toString().equals(""))
									break;
							}
							
							for(int j = 18; j < din; j+=4){ // Lunch
								rw = sht.getRow(j);
								cll = rw.getCell(2);
								cl1 = rw.getCell(3);
								cl2 = rw.getCell(4);
								if(cll != null && !cll.toString().equals("")){
									tmp.addLunch(new dish(cll.toString()));
									checkDish(cll.toString(), cl1.toString(), cl2.toString());
								}
							}
							
							rw = sht.getRow(din);
							while(rw != null){
								cll = rw.getCell(2);
								cl1 = rw.getCell(3);
								cl2 = rw.getCell(4);
								if(cll != null && !cll.toString().equals("")){
									tmp.addDinner(new dish(cll.toString()));
									checkDish(cll.toString(), cl1.toString(), cl2.toString());
								}
								din += 4;
								rw = sht.getRow(din);
							};
							today.add(tmp);
						}
						inp.close();
					}catch(IOException e){
						StringWriter er = new StringWriter();
						e.printStackTrace(new PrintWriter(er));
						JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
					}
				loadCombo(comboBox);
				}
		});
		btnImport.setBounds(217, 143, 87, 23);
		imExport.add(btnImport);
		
		final JToggleButton btnPrepare = new JToggleButton("\u5099\u6599");
		btnPrepare.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(!btnPrepare.isSelected() && btnPrepare.getText().equals("\u5099\u6599\u78BA\u8A8D")){
					if(JOptionPane.showConfirmDialog(null, "將會移除以儲存之備料資訊，要繼續嗎?", "Warning", JOptionPane.YES_NO_OPTION) == 0){
						btnPrepare.setText("備料");
						btnPrepare.setSelected(false);
						clearPrepare();
					}else{
						btnPrepare.setText("備料確認");
						btnPrepare.setSelected(true);
					}
				}else{
					btnPrepare.setText("備料確認");
					countPrepare(true);
				}
			}
		});
		btnPrepare.setBounds(257, 271, 102, 23);
		imExport.add(btnPrepare);
		
		JButton export = new JButton("\u532F\u51FA");
		export.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				XSSFWorkbook wbout = new XSSFWorkbook();
				
				CellStyle[] st = new CellStyle[7];
				for(int tmp = 0; tmp < 7; tmp++)
					st[tmp] = wbout.createCellStyle();
				st[0].setBorderTop(CellStyle.BORDER_THIN);
				st[0].setBorderLeft(CellStyle.BORDER_THIN);
				st[0].setBorderRight(CellStyle.BORDER_THIN);
				st[1].setBorderBottom(CellStyle.BORDER_THIN);
				st[1].setBorderLeft(CellStyle.BORDER_THIN);
				st[1].setBorderRight(CellStyle.BORDER_THIN);
				st[1].setBorderTop(CellStyle.BORDER_THIN);
				st[2].setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				st[2].setFillPattern(CellStyle.SOLID_FOREGROUND);
				st[2].setBorderLeft(CellStyle.BORDER_THIN);
				st[2].setBorderRight(CellStyle.BORDER_THIN);
				st[2].setBorderTop(CellStyle.BORDER_THIN);
				st[2].setBorderBottom(CellStyle.BORDER_THIN);
				st[3].setBorderBottom(CellStyle.BORDER_THIN);
				st[3].setBorderLeft(CellStyle.BORDER_THIN);
				st[3].setBorderRight(CellStyle.BORDER_THIN);
				st[3].setBorderTop(CellStyle.BORDER_THIN);
				st[3].setWrapText(true);
				st[4].setBorderLeft(CellStyle.BORDER_THIN);
				st[4].setBorderRight(CellStyle.BORDER_THIN);
				/*st[5].setBorderLeft(CellStyle.BORDER_THIN);
				st[5].setBorderRight(CellStyle.BORDER_THIN);
				st[5].setFillForegroundColor(IndexedColors.ORANGE.getIndex());
				st[6].setBorderBottom(CellStyle.BORDER_THIN);
				st[6].setBorderLeft(CellStyle.BORDER_THIN);
				st[6].setBorderRight(CellStyle.BORDER_THIN);
				st[6].setBorderTop(CellStyle.BORDER_THIN);
				st[6].setFillForegroundColor(IndexedColors.ORANGE.getIndex());*/
				for(int tmp = 0; tmp < 7; tmp++){
					st[tmp].setAlignment(CellStyle.ALIGN_CENTER);
					st[tmp].setVerticalAlignment(CellStyle.VERTICAL_CENTER);
					XSSFFont ft = wbout.createFont();
					ft.setFontHeightInPoints((short) 10);
					ft.setFontName("新細明體");
					st[tmp].setFont(ft);
				}
				XSSFFont ft = wbout.createFont();
				ft.setFontHeightInPoints((short) 14);
				ft.setFontName("新細明體");
				st[3].setFont(ft);
				
				wbout = writeOrder(wbout);
				wbout = writeAll(wbout,st);
				mapMat();
				if(btnPrepare.isSelected())
					wbout = writePrepare(wbout,true);
				else
					wbout = writePrepare(wbout,false);
				wbout = writeAddMinus(wbout);
				for(int shtnum = 0; shtnum < today.size(); shtnum++){
					XSSFSheet sht = wbout.createSheet(today.get(shtnum).getName());
					int rwnum = 0;
					
					sht.setDefaultRowHeightInPoints(35);
					sht.setColumnWidth(0, 1152);
					sht.setColumnWidth(1, 3840);
					sht.setColumnWidth(2, 4961);
					sht.setColumnWidth(3, 8640);
					sht.setColumnWidth(4, 7905);
					
					Row rw = sht.createRow(rwnum++);
					Cell cll = rw.createCell(0);
					cll.setCellValue(today.get(shtnum).getOrder()?"出餐":"不出");
					cll = rw.createCell(1);
					cll.setCellValue(today.get(shtnum).getShip()?"二送":"一送");
					
					rw = sht.createRow(rwnum++);
					cll = rw.createCell(0); cll.setCellValue("餐別");
					cll = rw.createCell(1); cll.setCellValue("類別");
					cll = rw.createCell(2); cll.setCellValue("名稱");
					cll = rw.createCell(3); cll.setCellValue("材料");
					sht.addMergedRegion(new CellRangeAddress(rwnum-1,rwnum-1,3,4));
					cll = rw.createCell(5); cll.setCellValue("飲食注意事項");
					
					rw = sht.createRow(rwnum++);
					cll = rw.createCell(0); cll.setCellValue("開胃菜");
					sht.addMergedRegion(new CellRangeAddress(rwnum-1,rwnum-1,0,1));
					cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getTomato()?"":"不蕃茄");
					cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getTomatoCmt());
					
					rw = sht.createRow(rwnum++);
					cll = rw.createCell(0); cll.setCellValue("養生茶飲");
					sht.addMergedRegion(new CellRangeAddress(rwnum-1,rwnum-1,0,1));
					if(today.get(shtnum).emptyDrink()){
						cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getDrink(0).getTasName());
						cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getDrink(0).getDishName());
						cll = rw.createCell(4); cll.setCellValue(today.get(shtnum).getDrink(0).getSize()?"全":"半");
						cll = rw.createCell(5); cll.setCellValue(today.get(shtnum).getDrink(0).getComment());
					}
					
					rw = sht.createRow(rwnum++);
					cll = rw.createCell(0); cll.setCellValue("早餐");
					cll = rw.createCell(1); cll.setCellValue("主食");
					if(today.get(shtnum).emptyBkfast()){
						cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getBkfast(0).getTasName());
						cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getBkfast(0).getDishName());
						cll = rw.createCell(4); cll.setCellValue(today.get(shtnum).getBkfast(0).getSize()?"全":"半");
						cll = rw.createCell(5); cll.setCellValue(today.get(shtnum).getBkfast(0).getComment());
					}
					
					rw = sht.createRow(rwnum++);
					sht.addMergedRegion(new CellRangeAddress(rwnum-2,rwnum-1,0,0));
					cll = rw.createCell(1); cll.setCellValue("甜點");
					if(today.get(shtnum).emptySweet()){
						cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getSweet(0).getTasName());
						cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getSweet(0).getDishName());
						cll = rw.createCell(4); cll.setCellValue(today.get(shtnum).getSweet(0).getSize()?"全":"半");
						cll = rw.createCell(5); cll.setCellValue(today.get(shtnum).getSweet(0).getComment());
					}
					
					if(today.get(shtnum).emptyLunch()){
						int tmp = rwnum;
						for(int idx = 0; idx < today.get(shtnum).getLunchCount(); idx++){
							rw = sht.createRow(rwnum++);
							cll = rw.createCell(0); cll.setCellValue("午餐");
							cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getLunch(idx).getTasName());
							cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getLunch(idx).getDishName());
							cll = rw.createCell(4); cll.setCellValue(today.get(shtnum).getLunch(idx).getSize()?"全":"半");
							cll = rw.createCell(5); cll.setCellValue(today.get(shtnum).getLunch(idx).getComment());
						}
						sht.addMergedRegion(new CellRangeAddress(tmp,rwnum-1,0,0));
					}
					
					if(today.get(shtnum).emptyDinner()){
						int tmp = rwnum;
						for(int idx = 0; idx < today.get(shtnum).getDinnerCount(); idx++){
							rw = sht.createRow(rwnum++);
							cll = rw.createCell(0); cll.setCellValue("晚餐");
							cll = rw.createCell(2); cll.setCellValue(today.get(shtnum).getDinner(idx).getTasName());
							cll = rw.createCell(3); cll.setCellValue(today.get(shtnum).getDinner(idx).getDishName());
							cll = rw.createCell(4); cll.setCellValue(today.get(shtnum).getDinner(idx).getSize()?"全":"半");
							cll = rw.createCell(5); cll.setCellValue(today.get(shtnum).getDinner(idx).getComment());
						}
						sht.addMergedRegion(new CellRangeAddress(tmp,rwnum-1,0,0));
					}
					
					if(!today.get(shtnum).getOrder())
						sht.setTabColor(2);
				}
				
				try {
					//String newtmp = address.getText();
					//int indx = newtmp.lastIndexOf("\\");
					//FileOutputStream fout = new FileOutputStream(newtmp.substring(0, indx+1) + oName.getText() + ".xlsx");
					FileOutputStream fout = new FileOutputStream(oName.getText() + ".xlsx");
					wbout.write(fout);
					fout.close();
				} catch (IOException e) {
					StringWriter er = new StringWriter();
					e.printStackTrace(new PrintWriter(er));
					JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		export.setBounds(513, 311, 87, 23);
		imExport.add(export);
		
		JButton load = new JButton("\u8F09\u5165");
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					InputStream inp = new FileInputStream(oName.getText() + ".xlsx");
					XSSFWorkbook wb = new XSSFWorkbook(inp);
					
					Sheet sht = wb.getSheetAt(2);
					Row rw = sht.getRow(2);
					if(rw != null && !rw.getCell(0).equals("")){
						btnPrepare.setSelected(true);
						btnPrepare.setText("備料確認");
						int rwnum = 2;
						while(rw != null && !rw.getCell(0).equals("")){
							int[] full;
							int[] half;
							full = new int[2];
							half = new int[2];
							for(int i = 0; i < 2; i++){
								String tmp = rw.getCell(i+3).getStringCellValue();
								int idx = tmp.indexOf("全");
								if(idx >= 0)
									full[i] = Integer.valueOf(tmp.substring(0, idx));
								int idx2 = tmp.indexOf("半");
								if(idx2 >= 0)
									half[i] = Integer.valueOf(tmp.substring(idx+1,idx2));
							}
							pList.add(new prepareItem(rw.getCell(0).getStringCellValue()
									,rw.getCell(1).getStringCellValue(),rw.getCell(2).getStringCellValue()
									,full[0]-full[1], half[0]-half[1],full[1],half[1]));
							rw = sht.getRow(++rwnum);
						}
					}
					
					for(int shtnum = 4; shtnum < wb.getNumberOfSheets(); shtnum++){
						sht = wb.getSheetAt(shtnum);
						patient tmp = new patient(sht.getSheetName());
						rw = sht.getRow(0);
						Cell cll = rw.getCell(0); // 出不出餐
						tmp.setOrder(cll.toString().equals("出餐"));
						cll = rw.getCell(1); // 幾送
						tmp.setShip(cll.toString().equals("二送"));
						rw = sht.getRow(2);
						cll = rw.getCell(2); // 蕃茄
						tmp.setTomato(!cll.toString().equals("不蕃茄"));
						cll = rw.getCell(3);
						tmp.setTomatoCmt(cll.toString());
						
						rw = sht.getRow(3); // 養生茶飲
						cll = rw.getCell(2);
						if(cll != null){
							int tmptas = makeTas(cll.toString(), true);
							cll = rw.getCell(3);
							String tmpname = cll.toString();
							cll = rw.getCell(4);
							boolean tmpsize = cll.toString().equals("全");
							cll = rw.getCell(5);
							tmp.addDrink(new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize));
						}
						
						rw = sht.getRow(4); // 早餐
						cll = rw.getCell(2);
						if(cll != null){
							int tmptas = makeTas(cll.toString(),false);
							cll = rw.getCell(3);
							String tmpname = cll.toString();
							cll = rw.getCell(4);
							boolean tmpsize = cll.toString().equals("全");
							cll = rw.getCell(5);
							tmp.addBkfast(new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize));
						}
						
						rw = sht.getRow(5); // 甜點
						cll = rw.getCell(2);
						if(cll != null){
							int tmptas = makeTas(cll.toString(),true);
							cll = rw.getCell(3);
							String tmpname = cll.toString();
							cll = rw.getCell(4);
							boolean tmpsize = cll.toString().equals("全");
							cll = rw.getCell(5);
							tmp.addSweet(new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize));
						}
						
						
						rw = sht.getRow(6);
						cll = rw.getCell(0);
						if(cll.toString().equals("午餐")){
							int dinIdx = 6;
							rw = sht.getRow(dinIdx);
							cll = rw.getCell(0);
							while(!cll.toString().equals("晚餐")){
								dinIdx++;
								rw = sht.getRow(dinIdx);
								if(rw == null) break;
								else cll = rw.getCell(0);
							}
							for(int i = 6; i < dinIdx; i++){
								rw = sht.getRow(i);
								cll = rw.getCell(2);
								int tmptas = makeTas(cll.toString(), false);
								cll = rw.getCell(3);
								String tmpname = cll.toString();
								cll = rw.getCell(4);
								boolean tmpsize = cll.toString().equals("全");
								cll = rw.getCell(5);
								tmp.addLunch((new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize)));
							}
							rw = sht.getRow(dinIdx++);
							while(rw != null){
								cll = rw.getCell(2);
								int tmptas = makeTas(cll.toString(), false);
								cll = rw.getCell(3);
								String tmpname = cll.toString();
								cll = rw.getCell(4);
								boolean tmpsize = cll.toString().equals("全");
								cll = rw.getCell(5);
								tmp.addDinner((new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize)));
								rw = sht.getRow(dinIdx++);
							}
						}else{
							int dinIdx = 6;
							rw = sht.getRow(dinIdx++);
							while(rw != null){
								cll = rw.getCell(2);
								int tmptas = makeTas(cll.toString(), false);
								cll = rw.getCell(3);
								String tmpname = cll.toString();
								cll = rw.getCell(4);
								boolean tmpsize = cll.toString().equals("全");
								cll = rw.getCell(5);
								tmp.addDinner((new dish(tmpname, tmptas, (cll!=null?cll.toString():""), tmpsize)));
								rw = sht.getRow(dinIdx++);
							}
						}
						
						today.add(tmp);
						checkList(tmp);
						loadCombo(comboBox);
					}
					readOrder(wb.getSheetAt(0));
					inp.close();
					
				}catch(IOException e1){
					StringWriter er = new StringWriter();
					e1.printStackTrace(new PrintWriter(er));
					JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		load.setBounds(513, 271, 87, 23);
		imExport.add(load);
		
		JLabel lblnnn = new JLabel("\u5982\u8981\u958B\u555F\u65E2\u6709\u7684\u820A\u7248\u83DC\u55AE\uFF0C\u8ACB\u9EDE\u9078\u5DE6\u908A\u7684\u958B\u555F\uFF0C\u4E26\u65BC");
		lblnnn.setVerticalAlignment(SwingConstants.TOP);
		lblnnn.setBounds(324, 59, 305, 21);
		imExport.add(lblnnn);
		
		JLabel label_1 = new JLabel("\u6E05\u55AE\u4E2D\u9078\u53D6\u6240\u8981\u7684\u500B\u4EBA\u83DC\u55AE\uFF0C\u53EF\u8907\u9078\uFF0C\u800C\u5F8C\u6309\u4E0B\u532F\u5165");
		label_1.setVerticalAlignment(SwingConstants.TOP);
		label_1.setBounds(324, 80, 305, 21);
		imExport.add(label_1);
		
		JLabel label_2 = new JLabel("\u5982\u679C\u8981\u958B\u555F\u6216\u532F\u51FA\u65B0\u7248\u7E3D\u8868\u53CA\u83DC\u55AE\uFF0C\u8ACB\u4F7F\u7528\u4E0B\u5217\u9078\u9805");
		label_2.setVerticalAlignment(SwingConstants.TOP);
		label_2.setBounds(324, 180, 305, 21);
		imExport.add(label_2);
		
		JLabel label_15 = new JLabel("\u8ACB\u65BC\u683C\u4E2D\u8F38\u5165\u6B32\u958B\u555F\u6216\u532F\u51FA\u4E4B\u6A94\u6848\u540D\u7A31\u3002\u82E5\u662F\u5B8C\u6210\u5099");
		label_15.setVerticalAlignment(SwingConstants.TOP);
		label_15.setBounds(324, 201, 305, 21);
		imExport.add(label_15);
		
		JLabel label_16 = new JLabel("\u6599\uFF0C\u8ACB\u6309\u4E0B\u5DE6\u5074\u6309\u9215\uFF0C\u52A0\u9000\u6599\u6703\u4EE5\u6309\u4E0B\u7576\u6642\u7684\u7D00\u9304\u4F5C");
		label_16.setVerticalAlignment(SwingConstants.TOP);
		label_16.setBounds(324, 222, 305, 21);
		imExport.add(label_16);
		
		JLabel label_17 = new JLabel("\u57FA\u6E96\u3002\u53EF\u91CD\u65B0\u5B8C\u6210\u5099\u6599\uFF0C\u4F46\u8ACB\u6CE8\u610F\u4F7F\u7528\uFF0C\u4EE5\u514D\u8AA4\u7528");
		label_17.setVerticalAlignment(SwingConstants.TOP);
		label_17.setBounds(324, 243, 305, 21);
		imExport.add(label_17);
		
		JLabel label_3 = new JLabel("\u51FA\u9910");
		label_3.setBounds(180, 15, 30, 15);
		personal.add(label_3);
		
		JLabel label_4 = new JLabel("\u4E00\u9001");
		label_4.setBounds(112, 44, 30, 15);
		personal.add(label_4);
		
		JLabel label_5 = new JLabel("\u4E8C\u9001");
		label_5.setBounds(180, 44, 30, 15);
		personal.add(label_5);
		
		JLabel label_6 = new JLabel("\u4E0D\u51FA");
		label_6.setBounds(112, 15, 30, 15);
		personal.add(label_6);
		
		JLabel label_7 = new JLabel("\u5348\u9910\u83DC\u55AE");
		label_7.setBounds(104, 134, 67, 15);
		personal.add(label_7);
		
		JLabel label_8 = new JLabel("\u665A\u9910\u83DC\u55AE");
		label_8.setBounds(434, 134, 67, 15);
		personal.add(label_8);
		
		JLabel label_9 = new JLabel("\u990A\u751F\u8336\u98F2");
		label_9.setHorizontalAlignment(SwingConstants.RIGHT);
		label_9.setBounds(201, 81, 60, 15);
		personal.add(label_9);
		
		JLabel label_10 = new JLabel("\u65E9\u9910");
		label_10.setBounds(235, 15, 30, 15);
		personal.add(label_10);
		
		JLabel label_11 = new JLabel("\u751C\u9EDE");
		label_11.setBounds(235, 46, 30, 15);
		personal.add(label_11);
		
		JPanel finalCheck = new JPanel();
		tabbedPane.addTab("\u4FEE\u6539 / \u6AA2\u67E5", null, finalCheck, null);
		finalCheck.setLayout(null);
		
		addressC = new JTextField();
		addressC.setEditable(false);
		addressC.setBounds(10, 10, 294, 21);
		finalCheck.add(addressC);
		addressC.setColumns(10);
		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(10, 59, 185, 286);
		finalCheck.add(scrollPane2);
		final JList<String> list2 = new JList<String>(listModel2);
		scrollPane2.setViewportView(list2);
		
		final JComboBox origCombo = new JComboBox();
		origCombo.setBounds(364, 146, 113, 21);
		updateTotalDish(origCombo);
		finalCheck.add(origCombo);
		
		final JComboBox newCombo = new JComboBox();
		newCombo.setBounds(516, 146, 113, 21);
		updateTotalDish(newCombo);
		finalCheck.add(newCombo);
		
		JButton btnOpenC = new JButton("\u958B\u555F");
		btnOpenC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listModel2.removeAllElements();
				JFileChooser fch = new JFileChooser(System.getProperty("user.dir"));
				FileNameExtensionFilter fil = new FileNameExtensionFilter("XLS* files","xlsx","xls");
				fch.setFileFilter(fil);
				int returnVal = fch.showOpenDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					addressC.setText(fch.getSelectedFile().getAbsolutePath());
					try{
						InputStream inp = new FileInputStream(fch.getSelectedFile().getAbsolutePath());
						XSSFWorkbook wb = new XSSFWorkbook(inp);
						/*Sheet sht = wb.getSheetAt(7);
						//for(int i = 0; i < sht.getPhysicalNumberOfRows(); i++){
						//Row rw = sht.getRow(i);
						Row rw = sht.getRow(18);
						Cell cll = rw.getCell(0);
						//System.out.println(cll.toString());
						textField_1.setText(cll.toString());
						//}*/
						
						for(int i = 0; i < wb.getNumberOfSheets(); i++)
							listModel2.addElement(wb.getSheetName(i));
						
						inp.close();
					}catch(IOException e){
						StringWriter er = new StringWriter();
						e.printStackTrace(new PrintWriter(er));
						JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
					}
					// textField_1.setText("You select: " + fch.getSelectedFile().getAbsolutePath());
					updateTotalDish(origCombo);
					updateTotalDish(newCombo);
				}
			}
		});
		btnOpenC.setBounds(217, 57, 87, 23);
		finalCheck.add(btnOpenC);
		
		JButton btnImportC = new JButton("\u6578\u91CF\u6AA2\u67E5");
		btnImportC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!list2.isSelectionEmpty())
					try{
						InputStream inp = new FileInputStream(addressC.getText());
						XSSFWorkbook wb = new XSSFWorkbook(inp);
						int i = list2.getSelectedIndex();
						//for(int i : list.getSelectedIndices()){
						Sheet sht = wb.getSheetAt(i);
						
						ArrayList<checkNumber> cList = new ArrayList<checkNumber>();
						
						ArrayList<Integer> rwidx = new ArrayList<Integer>();
						/*for(int j = 0; j < sht.getNumMergedRegions(); j++)
							if(sht.getMergedRegion(j).getFirstColumn() == 4)
								rwidx.add(sht.getMergedRegion(j).getFirstRow());
						Collections.sort(rwidx);*/
						for(int j = 2; j < sht.getPhysicalNumberOfRows(); j++){
							if(sht.getRow(j) == null) continue;
							if(sht.getRow(j).getCell(5) == null) continue;
							if(sht.getRow(j).getCell(5).getCellStyle().getBorderTop() == 1 ||
									sht.getRow(j-1).getCell(5).getCellStyle().getBorderBottom() == 1)
								rwidx.add(j);
						}
						
						for(int tmp = 0; tmp < today.size(); tmp++)
							if(today.get(tmp).getOrder())
								cList.add(new checkNumber(today.get(tmp).getName()));
						/*
						int rwnum = 2;
						while(rwnum < sht.getPhysicalNumberOfRows()){
							Row rw = sht.getRow(rwnum++);
							if(rw == null) continue;
							for(int col = 6; col < 16; col++){
								Cell cll = rw.getCell(col);
								if(cll == null || cll.toString().equals("")) continue;
								
								for(int cnum = 0; cnum < cList.size(); cnum++)
									if(cList.get(cnum).getName().equals(cll.toString()))
										cList.get(cnum).pp();
							}
						}*/
						
						for(int rwnum : rwidx){
							Row rw = sht.getRow(rwnum);
							for(int col = 6; col < 16; col++){
								Cell cll = rw.getCell(col);
								if(cll == null || cll.toString().equals("") || cll.toString().contains("晚餐") ||
										cll.toString().contains("晚上") || cll.toString().contains("午餐") ||
										cll.toString().contains("中午") || cll.getCellType() == 0) continue;
								
								boolean flg = true;
								for(int cnum = 0; cnum < cList.size(); cnum++)
									if(cList.get(cnum).getName().equals(cll.toString())){
										cList.get(cnum).pp();
										flg = false;
									}
								if(flg){
									cList.add(new checkNumber(cll.toString()));
									cList.get(cList.size()-1).pp();
								}
							}
						}
						
						String omsg;
						for(int clnum = 0; clnum < cList.size(); clnum++){
							boolean flg = true;
							for(int tdnum = 0; tdnum < today.size(); tdnum++)
								if(cList.get(clnum).getName().equals(today.get(tdnum).getName())){
									flg = false;
									if(cList.get(clnum).getCount()!=today.get(tdnum).getCount()){
										omsg = cList.get(clnum).getName() + "的數量不對，原有" + String.valueOf(today.get(tdnum).getCount()) + 
												"，現在卻有" + String.valueOf(cList.get(clnum).getCount());
										JOptionPane.showMessageDialog(null, omsg, "Modification Error", JOptionPane.WARNING_MESSAGE);
									}
									break;
								}
							if(flg){
								omsg = cList.get(clnum).getName() + "的數量不對，原有0，現在卻有" + String.valueOf(cList.get(clnum).getCount());
								JOptionPane.showMessageDialog(null, omsg, "Modification Error", JOptionPane.WARNING_MESSAGE);
							}
						}
						JOptionPane.showMessageDialog(null, "沒問題了！！！", "Done", JOptionPane.WARNING_MESSAGE);
						inp.close();
					}catch(IOException e){
						StringWriter er = new StringWriter();
						e.printStackTrace(new PrintWriter(er));
						JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
					}
				}
		});
		btnImportC.setBounds(217, 117, 87, 23);
		finalCheck.add(btnImportC);
		
		final JLabel modName = new JLabel("\u75C5\u60A3\u540D");
		modName.setFont(new Font("標楷體", Font.BOLD, 15));
		modName.setBounds(364, 26, 73, 22);
		finalCheck.add(modName);
		
		final JLabel origDishName = new JLabel("\u539F\u83DC\u540D");
		origDishName.setBounds(364, 62, 113, 15);
		finalCheck.add(origDishName);
		
		newDishName = new JTextField();
		newDishName.setText("\u65B0\u83DC\u540D");
		newDishName.setBounds(365, 87, 210, 21);
		finalCheck.add(newDishName);
		newDishName.setColumns(10);
		
		JButton btnSkip = new JButton("\u4E0D\u63DB");
		btnSkip.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dfList.remove(0);
				updateMod(modName, origDishName, newDishName);
			}
		});
		btnSkip.setBounds(584, 10, 61, 39);
		finalCheck.add(btnSkip);
		
		JButton btnChange = new JButton("\u78BA\u5B9A");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				checkDish(dfList.get(0).getNName(),dfList.get(0).getCmt1(),dfList.get(0).getCmt2());
				for(int pn = 0; pn < today.size(); pn++)
					if(today.get(pn).getName().equals(dfList.get(0).getPName())){
						switch(dfList.get(0).getMeal()){
						case 1:
							today.get(pn).getDrink(0).setName(dfList.get(0).getNName());
							today.get(pn).getDrink(0).setMat(dfList.get(0).getCmt1(), dfList.get(0).getCmt2());
							break;
						case 2:
							today.get(pn).getBkfast(0).setName(dfList.get(0).getNName());
							today.get(pn).getBkfast(0).setMat(dfList.get(0).getCmt1(), dfList.get(0).getCmt2());
							break;
						case 3:
							today.get(pn).getSweet(0).setName(dfList.get(0).getNName());
							today.get(pn).getSweet(0).setMat(dfList.get(0).getCmt1(), dfList.get(0).getCmt2());
							break;
						case 4:
							for(int l = 0; l < today.get(pn).getLunchCount(); l++)
								if(today.get(pn).getLunch(l).getDishName().equals(dfList.get(0).getOName())){
									today.get(pn).getLunch(l).setName(dfList.get(0).getNName());
									today.get(pn).getLunch(l).setMat(dfList.get(0).getCmt1(), dfList.get(0).getCmt2());
								}
							break;
						case 5:
							for(int d = 0; d < today.get(pn).getDinnerCount(); d++)
								if(today.get(pn).getDinner(d).getDishName().equals(dfList.get(0).getOName())){
									today.get(pn).getDinner(d).setName(dfList.get(0).getNName());
									today.get(pn).getDinner(d).setMat(dfList.get(0).getCmt1(), dfList.get(0).getCmt2());
								}
							break;
						}
						checkList(today.get(pn));
						break;
					}
				dfList.remove(0);
				updateMod(modName, origDishName, newDishName);
			}
		});
		btnChange.setBounds(584, 70, 61, 39);
		finalCheck.add(btnChange);
		
		JButton btnMod = new JButton("\u4FEE\u6539");
		btnMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!list2.isSelectionEmpty())
					try{
						InputStream inp = new FileInputStream(addressC.getText());
						XSSFWorkbook wb = new XSSFWorkbook(inp);
						
						for(int i : list2.getSelectedIndices()){
							Sheet sht = wb.getSheetAt(i);
							for(int p = 0; p < today.size(); p++)
								if(sht.getSheetName().contains(today.get(p).getName())){
									if(today.get(p).emptyDrink())
										if(!sht.getRow(6).getCell(2).toString().equals(today.get(p).getDrink(0).getDishName()))
											dfList.add(new diffDish(today.get(p).getName(), 1, today.get(p).getDrink(0).getDishName(),
											sht.getRow(6).getCell(2).toString(),sht.getRow(6).getCell(3).toString(),
											sht.getRow(6).getCell(4).toString()));
									if(today.get(p).emptyBkfast())
										if(!sht.getRow(10).getCell(2).toString().equals(today.get(p).getBkfast(0).getDishName()))
											dfList.add(new diffDish(today.get(p).getName(), 2, today.get(p).getBkfast(0).getDishName(),
													sht.getRow(10).getCell(2).toString(), sht.getRow(10).getCell(3).toString(),
													sht.getRow(10).getCell(4).toString()));
									if(today.get(p).emptySweet())
										if(!sht.getRow(14).getCell(2).toString().equals(today.get(p).getSweet(0).getDishName()))
											dfList.add(new diffDish(today.get(p).getName(), 3, today.get(p).getSweet(0).getDishName(),
													sht.getRow(14).getCell(2).toString(), sht.getRow(14).getCell(3).toString(),
													sht.getRow(14).getCell(4).toString()));
									if(today.get(p).emptyLunch())
										for(int lp = 0; lp < today.get(p).getLunchCount(); lp++)
											if(!sht.getRow(18+4*lp).getCell(2).toString().equals(today.get(p).getLunch(lp).getDishName()))
												dfList.add(new diffDish(today.get(p).getName(), 4, today.get(p).getLunch(lp).getDishName(),
													sht.getRow(18+4*lp).getCell(2).toString(), sht.getRow(18+4*lp).getCell(3).toString(),
													sht.getRow(18+4*lp).getCell(4).toString()));
									
									int dinNum;
									for(dinNum = 19; dinNum < 66; dinNum++)
										if(sht.getRow(dinNum).getCell(0) != null && sht.getRow(dinNum).getCell(0).toString().length()>0)
											break;
									if(today.get(p).emptyDinner())
										for(int dp = 0; dp < today.get(p).getDinnerCount(); dp++)
											if(!sht.getRow(dinNum+4*dp).getCell(2).toString().equals(today.get(p).getDinner(dp).getDishName()))
												dfList.add(new diffDish(today.get(p).getName(), 5, today.get(p).getDinner(dp).getDishName(),
													sht.getRow(dinNum+4*dp).getCell(2).toString(), sht.getRow(dinNum+4*dp).getCell(3).toString(),
													sht.getRow(dinNum+4*dp).getCell(4).toString()));
								}
							updateMod(modName, origDishName, newDishName);
						}
						inp.close();
					}catch(IOException e){
						StringWriter er = new StringWriter();
						e.printStackTrace(new PrintWriter(er));
						JOptionPane.showMessageDialog(null, er.toString(), "Error Message", JOptionPane.WARNING_MESSAGE);
					}
				loadCombo(comboBox);
				}
		});
		btnMod.setBounds(217, 176, 87, 23);
		finalCheck.add(btnMod);
		
		JLabel label_19 = new JLabel("\u7684");
		label_19.setFont(new Font("標楷體", Font.BOLD, 15));
		label_19.setBounds(425, 28, 24, 18);
		finalCheck.add(label_19);
		
		JLabel label_20 = new JLabel("\u63DB\u6210");
		label_20.setFont(new Font("標楷體", Font.BOLD, 15));
		label_20.setBounds(498, 61, 47, 15);
		finalCheck.add(label_20);
		
		JLabel lblor = new JLabel("-------------------- OR --------------------");
		lblor.setFont(new Font("新細明體", Font.BOLD, 14));
		lblor.setBounds(375, 118, 241, 15);
		finalCheck.add(lblor);
		
		JLabel label_21 = new JLabel("\u63DB\u6210");
		label_21.setFont(new Font("標楷體", Font.BOLD, 15));
		label_21.setBounds(480, 148, 37, 15);
		finalCheck.add(label_21);
		
		JButton btnChangeAll = new JButton("\u5168\u90E8\u66FF\u63DB");
		btnChangeAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int orig = origCombo.getSelectedIndex();
				int next = newCombo.getSelectedIndex();
				while(true){
					for(int p = 0; p < today.size(); p++){
						if(today.get(p).emptyDrink())
							if(today.get(p).getDrink(0).getDishName().equals(dishMat.get(orig).getDishName())){
								today.get(p).getDrink(0).setName(dishMat.get(next).getDishName());
								today.get(p).getDrink(0).setMat(dishMat.get(next).getMat1(), dishMat.get(next).getMat2());
							}
						if(today.get(p).emptyBkfast())
							if(today.get(p).getBkfast(0).getDishName().equals(dishMat.get(orig).getDishName())){
								today.get(p).getBkfast(0).setName(dishMat.get(next).getDishName());
								today.get(p).getBkfast(0).setMat(dishMat.get(next).getMat1(), dishMat.get(next).getMat2());
							}
						if(today.get(p).emptySweet())
							if(today.get(p).getSweet(0).getDishName().equals(dishMat.get(orig).getDishName())){
								today.get(p).getSweet(0).setName(dishMat.get(next).getDishName());
								today.get(p).getSweet(0).setMat(dishMat.get(next).getMat1(), dishMat.get(next).getMat2());
							}
						if(today.get(p).emptyLunch())
							for(int l = 0; l < today.get(p).getLunchCount(); l++)
								if(today.get(p).getLunch(l).getDishName().equals(dishMat.get(orig).getDishName())){
									today.get(p).getLunch(l).setName(dishMat.get(next).getDishName());
									today.get(p).getLunch(l).setMat(dishMat.get(next).getMat1(), dishMat.get(next).getMat2());
								}
						if(today.get(p).emptyDinner())
							for(int d = 0; d < today.get(p).getDinnerCount(); d++)
								if(today.get(p).getDinner(d).getDishName().equals(dishMat.get(orig).getDishName())){
									today.get(p).getDinner(d).setName(dishMat.get(next).getDishName());
									today.get(p).getDinner(d).setMat(dishMat.get(next).getMat1(), dishMat.get(next).getMat2());
								}
						checkList(today.get(p));
					}
					
					// check around
					boolean flg = false;
					for(int p = 0; p < today.size(); p++){
						if(today.get(p).emptyDrink())
							if(today.get(p).getDrink(0).getDishName().equals(dishMat.get(orig).getDishName()))
								flg = true;
						if(today.get(p).emptyBkfast())
							if(today.get(p).getBkfast(0).getDishName().equals(dishMat.get(orig).getDishName()))
								flg = true;
						if(today.get(p).emptySweet())
							if(today.get(p).getSweet(0).getDishName().equals(dishMat.get(orig).getDishName()))
								flg = true;
						if(today.get(p).emptyLunch())
							for(int l = 0; l < today.get(p).getLunchCount(); l++)
								if(today.get(p).getLunch(l).getDishName().equals(dishMat.get(orig).getDishName()))
									flg = true;
						if(today.get(p).emptyDinner())
							for(int d = 0; d < today.get(p).getDinnerCount(); d++)
								if(today.get(p).getDinner(d).getDishName().equals(dishMat.get(orig).getDishName()))
									flg = true;
					}
					if(!flg) break;
				}
			}
		});
		btnChangeAll.setBounds(558, 180, 87, 23);
		finalCheck.add(btnChangeAll);
		
		JLabel lblNewLabel_1 = new JLabel("<html>\r\n\u8ACB\u5148\u958B\u555F\u4FEE\u6539\u5F8C\u7684\u6BD4\u8F03\u6A94\uFF0C\u53EF\u70BA\u4FEE\u6539\u5F8C\u7684\u7E3D\u8868\uFF0C\u6216\u662F\u4FEE\u6539\u5F8C\u7684\u500B\u4EBA\u83DC\u55AE\u3002<br>\r\n\u82E5\u8981\u6AA2\u67E5\u6BD4\u8F03\u6A94\u5167\u4E2D\u7E3D\u8868\u4E0A\u7684\u6578\u91CF\u662F\u5426\u6B63\u78BA\uFF0C\u8ACB\u9078\u64C7\u6BD4\u8F03\u6A94\u4E2D\u7684\u7E3D\u8868\uFF0C\u4E26\u6309\u4E0B\u6578\u91CF\u6AA2\u67E5\u3002<br>\r\n\u82E5\u8981\u4F9D\u7167\u4FEE\u6539\u5F8C\u7684\u500B\u4EBA\u83DC\u55AE\u505A\u4FEE\u6539\uFF0C\u8ACB\u9078\u53D6\u6709\u4FEE\u6539\u904E\u7684\u500B\u4EBA\u83DC\u55AE\uFF0C\u4E26\u57F7\u884C\u6309\u4E0B\u4FEE\u6539\u9375\uFF0C\u53F3\u4E0A\u65B9\u7684\u5340\u57DF\u5C07\u80FD\u5920\u9010\u9805\u8ACB\u4F7F\u7528\u8005\u78BA\u8A8D\u662F\u5426\u540C\u610F\u4FEE\u6539\uFF0C\u6216\u662F\u5C07\u67D0\u9053\u83DC\u5B8C\u5168\u66FF\u63DB\u6210\u53E6\u4E00\u9053\u3002\r\n</html>");
		lblNewLabel_1.setBounds(217, 213, 428, 142);
		finalCheck.add(lblNewLabel_1);
		
		JButton button = new JButton("\u8F38\u5165\u65B0\u83DC");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String dishName = (String)JOptionPane.showInputDialog(null,"請輸入新菜名稱","新增菜色",JOptionPane.QUESTION_MESSAGE);
				String dishMat1 = (String)JOptionPane.showInputDialog(null,"請輸入新菜材料(1)","新增菜色",JOptionPane.QUESTION_MESSAGE);
				String dishMat2 = (String)JOptionPane.showInputDialog(null,"請輸入新菜材料(2)","新增菜色",JOptionPane.QUESTION_MESSAGE);
				dishMat.add(new dish(dishName, dishMat1, dishMat2));
				JOptionPane.showMessageDialog(null, "已增加新菜色", "新增完成", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		button.setBounds(364, 180, 87, 23);
		finalCheck.add(button);
		
		JLabel lblCopyright = new JLabel("Copyright \u00A9 Yen-Ming Huang 2014 All Rights Reserved.");
		lblCopyright.setBounds(350, 390, 340, 15);
		contentPane.add(lblCopyright);
		
		JLabel label_18 = new JLabel("No action allowed without Yen-Ming Huang's consent.");
		label_18.setBounds(356, 405, 314, 15);
		contentPane.add(label_18);
		
		
		
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void loadCombo(JComboBox jcombo){
		String[] tmp = new String[today.size()+1];
		tmp[0] = "";
		for(int i = 0; i < today.size(); i++){
			if(today.get(i).getOrder())
				tmp[i+1] = today.get(i).getName();
			else
				tmp[i+1] = "! " + today.get(i).getName();
		}
		jcombo.removeAllItems();
		jcombo.setModel(new DefaultComboBoxModel(tmp));
	}
	
	public void delList(patient pnm){
		for(int i = dList.size()-1; i >= 0; i--){
			dList.get(i).removeList(pnm);
			if(dList.get(i).getSize() <= 0)
				dList.remove(i);
		}
		for(int i = lList.size()-1; i >= 0; i--){
			lList.get(i).removeList(pnm);
			if(lList.get(i).getSize() <= 0)
				lList.remove(i);
		}
		sortList();
	}
	
	public void addList(patient pnm, String dnm, boolean time, int ml){
		boolean flag = false;
		if(time){
			for(int i = 0; i < dList.size(); i++)
				if(dList.get(i).getName().equals(dnm)){
					flag = true;
					dList.get(i).addList(pnm, ml);
				}
			if(!flag){
				boolean flg = true;
				for(int i = 0; i < dishMat.size(); i++)
					if(dishMat.get(i).getDishName().equals(dnm)){
						flg = false;
						dList.add(new dishList(dnm, dishMat.get(i).getMat1(),dishMat.get(i).getMat2(), pnm, ml));
					}
				if(flg)
					dList.add(new dishList(dnm, "", "", pnm, ml));
			}
		}else{
			for(int i = 0; i < lList.size(); i++)
				if(lList.get(i).getName().equals(dnm)){
					flag = true;
					lList.get(i).addList(pnm, ml);
				}
			if(!flag){
				boolean flg = true;
				for(int i = 0; i < dishMat.size(); i++)
					if(dishMat.get(i).getDishName().equals(dnm)){
						flg = false;
						lList.add(new dishList(dnm, dishMat.get(i).getMat1(), dishMat.get(i).getMat2(), pnm, ml));
					}
				if(flg)
					lList.add(new dishList(dnm, "", "", pnm, ml));
			}
		}
	}
	
	public void checkList(patient inp){
		delList(inp);
		
		if(inp.getOrder()){
			// Tomato
			if(inp.getShip()){
				addList(inp,"開胃蕃茄",false, 0);
				addList(inp,"開胃蕃茄",true, 0);
				if(inp.emptySweet())
					addList(inp,inp.getSweet(0).getDishName(),false, 2);
				if(inp.emptyDrink())
					addList(inp,inp.getDrink(0).getDishName(),false, 1);
			}else{
				if(inp.emptyLunch()){
					addList(inp,"開胃蕃茄",false, 0);
					if(inp.emptySweet())
						addList(inp,inp.getSweet(0).getDishName(),false, 2);
					if(inp.emptyDrink())
						addList(inp,inp.getDrink(0).getDishName(),false, 1);
				}else{
					addList(inp,"開胃蕃茄",true, 0);
					if(inp.emptySweet()){
						addList(inp,inp.getSweet(0).getDishName(),false, 7);
						addList(inp,inp.getSweet(0).getDishName(),true, 2);
					}
					if(inp.emptyDrink()){
						addList(inp,inp.getDrink(0).getDishName(),false, 6);
						addList(inp,inp.getDrink(0).getDishName(),true, 1);
					}
				}
			}
			
			// 早午晚餐
			if(inp.getShip()){
				if(inp.emptyBkfast())
					addList(inp,inp.getBkfast(0).getDishName(),true, 3);
				for(int i = 0; i < inp.getLunchCount(); i++)
					addList(inp,inp.getLunch(i).getDishName(),false, 4);
				for(int i = 0; i < inp.getDinnerCount(); i++)
					addList(inp,inp.getDinner(i).getDishName(),true, 5);
			}else{
				if(inp.emptyLunch()){
					if(inp.emptyBkfast())
						addList(inp,inp.getBkfast(0).getDishName(),false, 3);
					for(int i = 0; i < inp.getLunchCount(); i++)
						addList(inp,inp.getLunch(i).getDishName(),false, 4);
					for(int i = 0; i < inp.getDinnerCount(); i++)
						addList(inp,inp.getDinner(i).getDishName(),false, 5);
				}else{
					if(inp.emptyBkfast())
						addList(inp,inp.getBkfast(0).getDishName(),true, 3);
					for(int i = 0; i < inp.getDinnerCount(); i++)
						addList(inp,inp.getDinner(i).getDishName(),true, 5);
				}
			}
		}
		sortList();
	}
	
	public void sortList(){
		boolean tmpflg = true;
		for(int i = 0; i < lList.size(); i++)
			if(lList.get(i).getName().equals("======分隔標題======"))
				tmpflg = false;
		for(int i = 0; i < mList.size(); i++)
			if(mList.get(i).getName().equals("======分隔標題======"))
				tmpflg = false;
		if(tmpflg)
			lList.add(new dishList("======分隔標題======","","",1));
		tmpflg = true;
		for(int i = 0; i < dList.size(); i++)
			if(dList.get(i).getName().equals("======分隔標題======"))
				tmpflg = false;
		for(int i = 0; i < mList.size(); i++)
			if(mList.get(i).getName().equals("======分隔標題======"))
				tmpflg = false;
		if(tmpflg)
			dList.add(new dishList("======分隔標題======","","",1));
		
		listModelL.removeAllElements();
		listModelD.removeAllElements();
		listModelM.removeAllElements();
		for(int i = 0; i < lList.size(); i++)
			listModelL.addElement(lList.get(i).getName());
		for(int i = 0; i < dList.size(); i++)
			listModelD.addElement(dList.get(i).getName());
		for(int i = 0; i < mList.size(); i++)
			listModelM.addElement(mList.get(i).getName());
	}
	
	public int makeTas(String strTas, boolean drink){
		if(drink){
			switch(strTas){
			case "無糖":
				return 6;
			case "少糖":
				return 7;
			case "正常":
				return 8;
			default:
				return 0;
			}
		}else{
			switch(strTas){
			case "不調味":
				return 0;
			case "淡淡":
				return 1;
			case "稍淡":
				return 2;
			case "正常":
				return 3;
			case "稍重":
				return 4;
			case "重":
				return 5;
			default:
				return 0;
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public XSSFWorkbook writeAll(XSSFWorkbook wb, CellStyle[] st){
		XSSFSheet sht = wb.createSheet("總表（出餐）");
		
		sht.getPrintSetup().setLandscape(true);
		sht.getPrintSetup().setPaperSize((short)9);
		
		sht.setDefaultRowHeightInPoints(13);
		sht.setColumnWidth(0, 3744);
		sht.setColumnWidth(1, 4000);
		sht.setColumnWidth(2, 6656);
		sht.setColumnWidth(3, 1536);
		sht.setColumnWidth(4, 1376);
		sht.setColumnWidth(5, 3104);
		for(int i = 6; i < 16; i++)
			sht.setColumnWidth(i, 1568);
		
		int rwnum = 0, colnum = 0;
		sht = markT(sht, rwnum, "中午出餐", st, true);
		RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum,rwnum,0,15), sht, wb);
		rwnum+=2;
		RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-1,rwnum-1,0,15), sht, wb);
		RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-1,rwnum-1,0,15), sht, wb);
		
		Row[] rw = new Row[4];
		Cell[] cll = new Cell[4];
		for(int tmp = 0; tmp < 4; tmp++){
			rw[tmp] = sht.createRow(rwnum++);
			cll[tmp] = rw[tmp].createCell(colnum);
		}
		
		// 蕃茄
		cll[0].setCellValue("開胃蕃茄");
		cll[0].setCellStyle(st[3]);
		colnum = 6;
		for(int tmp = 0; tmp < 4; tmp++)
			cll[tmp] = rw[tmp].createCell(colnum);
		for(int pListnum = 0; pListnum < lList.get(0).getSize(); pListnum++){
			if(lList.get(0).getItemOnList(pListnum).getPatient().getTomato())
				markName(cll, lList.get(0).getItemOnList(pListnum).getPatient().getName()
						,lList.get(0).getItemOnList(pListnum).getPatient().getTomatoCmt(),""
						,lList.get(0).getItemOnList(pListnum).getPatient().getTomatoStat(), st);
			else
				markName(cll, lList.get(0).getItemOnList(pListnum).getPatient().getName()
						,lList.get(0).getItemOnList(pListnum).getPatient().getTomatoStat(),
						lList.get(0).getItemOnList(pListnum).getPatient().getTomatoCmt(),"", st);
			colnum++;
			for(int tmp = 0; tmp < 4; tmp++)
				cll[tmp] = rw[tmp].createCell(colnum);
			if(colnum == 16){
				colnum = 6;
				for(int tmp = 0; tmp < 4; tmp++){
					rw[tmp] = sht.createRow(rwnum++);
					cll[tmp] = rw[tmp].createCell(colnum);
				}
				RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
				RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
				RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
				RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
			}
		}
		if(colnum > 6){
			colnum = 6;
			for(int tmp = 0; tmp < 4; tmp++){
				rw[tmp] = sht.createRow(rwnum++);
				cll[tmp] = rw[tmp].createCell(colnum);
			}
			RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
			RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
			RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
			RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
		}
		
		for(int i = 3; i < 5; i++)
			sht.addMergedRegion(new CellRangeAddress(2,rwnum-5,i,i));
		int tomatoRowIdx = rwnum-4;
		
		cll[0] = rw[0].createCell(1);
		cll[0].setCellValue("晚上出");
		cll[0].setCellStyle(st[3]);
		colnum = 6;
		for(int tmp = 0; tmp < 4; tmp++)
			cll[tmp] = rw[tmp].createCell(colnum);
		for(int pListnum = 0; pListnum < dList.get(0).getSize(); pListnum++){
			if(dList.get(0).getItemOnList(pListnum).getPatient().getTomato())
				markName(cll, dList.get(0).getItemOnList(pListnum).getPatient().getName()
						,dList.get(0).getItemOnList(pListnum).getPatient().getTomatoCmt(),""
						,dList.get(0).getItemOnList(pListnum).getPatient().getTomatoStat(), st);
			else
				markName(cll, dList.get(0).getItemOnList(pListnum).getPatient().getName()
						,dList.get(0).getItemOnList(pListnum).getPatient().getTomatoStat(),
						dList.get(0).getItemOnList(pListnum).getPatient().getTomatoCmt(),"", st);
			colnum++;
			for(int tmp = 0; tmp < 4; tmp++)
				cll[tmp] = rw[tmp].createCell(colnum);
			if(colnum == 16){
				colnum = 6;
				for(int tmp = 0; tmp < 4; tmp++){
					rw[tmp] = sht.createRow(rwnum++);
					cll[tmp] = rw[tmp].createCell(colnum);
				}
				RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
				RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
				RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
				RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
			}
		}
		if(colnum > 6){
			colnum = 6;
			for(int tmp = 0; tmp < 4; tmp++){
				rw[tmp] = sht.createRow(rwnum++);
				cll[tmp] = rw[tmp].createCell(colnum);
			}
			RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
			RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
			RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
			RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
		}
		
		sht.addMergedRegion(new CellRangeAddress(2,rwnum-5,0,0));
		for(int i = 1; i < 5; i++)
			sht.addMergedRegion(new CellRangeAddress(tomatoRowIdx,rwnum-5,i,i));
		
		
		// dish
		tomatoRowIdx = rwnum-4;
		for(int listnum = 1; listnum < lList.size(); listnum++){
			if(lList.get(listnum).getName().equals("======分隔標題======")){
				for(int tmpcol = 0; tmpcol<16; tmpcol++){
					RegionUtil.setBorderLeft(1, new CellRangeAddress(2,rwnum-5,tmpcol,tmpcol), sht, wb);
					RegionUtil.setBorderRight(1, new CellRangeAddress(2,rwnum-5,tmpcol,tmpcol), sht, wb);
				}
				rwnum-=4;
				for(int i = tomatoRowIdx; i < rwnum; i+=4)
					sht.addMergedRegion(new CellRangeAddress(i,i+3,4,4));
				markT(sht,rwnum++,"",st,false);
				tomatoRowIdx = rwnum;
				for(int tmp = 0; tmp < 4; tmp++){
					rw[tmp] = sht.createRow(rwnum++);
					cll[tmp] = rw[tmp].createCell(colnum);
				}
				continue;
			}
			int dishIdx = rwnum-4;
			cll[0] = rw[0].createCell(0);
			cll[0].setCellValue(lList.get(listnum).getName());
			cll[0].setCellStyle(st[3]);
			cll[0] = rw[0].createCell(1);
			cll[0].setCellValue(getMat1(lList.get(listnum).getName()));
			cll[0].setCellStyle(st[3]);
			cll[0] = rw[0].createCell(2);
			cll[0].setCellValue(getMat2(lList.get(listnum).getName()));
			cll[0].setCellStyle(st[3]);
			if(lList.get(listnum).isDrink()){
				for(int i = 6; i < 10; i++){
					int tasteIdx = rwnum-4;
					for(int sz = 1; sz >= 0; sz--){
						int count = 0;
						for(int j = 0; j < lList.get(listnum).getSize(); j++){
							if(lList.get(listnum).getItemOnList(j).getListItemTas()==i &&
									lList.get(listnum).getItemOnList(j).getListItemSize()==sz){
								cll[0] = (rw[0].getCell(3)==null)?rw[0].createCell(3):rw[0].getCell(3);//rw[0].createCell(3);
								cll[0].setCellValue(lList.get(listnum).getItemOnList(j).getListItemTasName());
								cll[0].setCellStyle(st[3]);
								/*for(int k = 0; k < 5; k++)
									sht.addMergedRegion(new CellRangeAddress(cll[0].getRowIndex(),cll[0].getRowIndex()+3,
											cll[0].getColumnIndex()+1-k,cll[0].getColumnIndex()+1-k));*/
								for(int tmp = 0; tmp < 4; tmp++)
									cll[tmp] = rw[tmp].createCell(colnum);
								markName(cll, lList.get(listnum).getItemOnList(j).getListItemName()
										,lList.get(listnum).getItemOnList(j).getListItemStat()
										,lList.get(listnum).getItemOnList(j).getListItemCmt(),"", st);
								colnum++;
								count++;
							}
							if(colnum == 16){
								cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
								cll[0].setCellValue(count);
								cll[0].setCellStyle(st[3]);
								count = 0;
								colnum = 6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
						if(colnum > 6){
							cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
							cll[0].setCellValue(count);
							cll[0].setCellStyle(st[3]);
							colnum = 6;
							for(int tmp = 0; tmp < 4; tmp++){
								rw[tmp] = sht.createRow(rwnum++);
								cll[tmp] = rw[tmp].createCell(colnum);
							}
							RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
							RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
							RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
							RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
						}
					}
					if(rwnum-4 > tasteIdx)
						sht.addMergedRegion(new CellRangeAddress(tasteIdx,rwnum-5,3,3));
					
				}
			}else{
				for(int i = 0; i < 6; i++){
					boolean flg_newline = true;
					int tmpcnt = 0;
					for(int j = 0; j < lList.get(listnum).getSize(); j++)
						if(lList.get(listnum).getItemOnList(j).getListItemTas() == i)
							tmpcnt++;
					if(tmpcnt < 11) flg_newline = false;
					int tasteIdx = rwnum-4;
					for(int sz = 1; sz >= 0; sz--){
						if(sz == 0) flg_newline = true;
						int count = 0;
						for(int j = 0; j < lList.get(listnum).getSize(); j++){
							if(lList.get(listnum).getItemOnList(j).getListItemTas()==i &&
									lList.get(listnum).getItemOnList(j).getListItemSize()==sz){
								cll[0] = (rw[0].getCell(3)==null)?rw[0].createCell(3):rw[0].getCell(3);//rw[0].createCell(3);
								cll[0].setCellValue(lList.get(listnum).getItemOnList(j).getListItemTasName());
								cll[0].setCellStyle(st[3]);
								/*for(int k = 0; k < 5; k++)
									sht.addMergedRegion(new CellRangeAddress(cll[0].getRowIndex(),cll[0].getRowIndex()+3,cll[0].getColumnIndex()+1-k,cll[0].getColumnIndex()+1-k));*/
								for(int tmp = 0; tmp < 4; tmp++)
									cll[tmp] = rw[tmp].createCell(colnum);
								markName(cll, lList.get(listnum).getItemOnList(j).getListItemName()
										,lList.get(listnum).getItemOnList(j).getListItemStat()
										,lList.get(listnum).getItemOnList(j).getListItemCmt(),"", st);
								colnum++;
								count++;
							}
							if(colnum == 16){
								cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
								if(count > 0)
									cll[0].setCellValue(cll[0].getStringCellValue() + String.valueOf(count) + ((sz==0)?"半":"全"));
								cll[0].setCellStyle(st[3]);
								count = 0;
								colnum = 6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
						if(colnum > 6){
							cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
							if(count > 0)
								cll[0].setCellValue(cll[0].getStringCellValue() + String.valueOf(count) + ((sz==0)?"半":"全"));
							cll[0].setCellStyle(st[3]);
							if(flg_newline){
								colnum=6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
					}
					if(rwnum-4 > tasteIdx)
						sht.addMergedRegion(new CellRangeAddress(tasteIdx,rwnum-5,3,3));
					
				}
			}
			
			for(int i = 0; i < 3; i++)
				sht.addMergedRegion(new CellRangeAddress(dishIdx,rwnum-5,i,i));
		}
		
		for(int tmpcol = 0; tmpcol<16; tmpcol++){
			RegionUtil.setBorderLeft(1, new CellRangeAddress(2,rwnum-5,tmpcol,tmpcol), sht, wb);
			RegionUtil.setBorderRight(1, new CellRangeAddress(2,rwnum-5,tmpcol,tmpcol), sht, wb);
		}
		rwnum-=4;
		
		for(int i = tomatoRowIdx; i < rwnum; i+=4)
			sht.addMergedRegion(new CellRangeAddress(i,i+3,4,4));
		
		sht = markT(sht, rwnum, "晚上出餐", st, true);
		RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum,rwnum,0,15), sht, wb);
		rwnum+=2;
		int tmprow = rwnum;
		RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-1,rwnum-1,0,15), sht, wb);
		RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-1,rwnum-1,0,15), sht, wb);
		
		for(int tmp = 0; tmp < 4; tmp++){
			rw[tmp] = sht.createRow(rwnum++);
			cll[tmp] = rw[tmp].createCell(0);
		}
		for(int listnum = 1; listnum < dList.size(); listnum++){
			if(dList.get(listnum).getName().equals("======分隔標題======")){
				for(int tmpcol = 0; tmpcol<16; tmpcol++){
					RegionUtil.setBorderLeft(1, new CellRangeAddress(tmprow,rwnum-5,tmpcol,tmpcol), sht, wb);
					RegionUtil.setBorderRight(1, new CellRangeAddress(tmprow,rwnum-5,tmpcol,tmpcol), sht, wb);
				}
				rwnum-=4;
				for(int i = tmprow; i < rwnum; i+=4)
					sht.addMergedRegion(new CellRangeAddress(i,i+3,4,4));
				markT(sht,rwnum++,"",st,false);
				tmprow = rwnum;
				for(int tmp = 0; tmp < 4; tmp++){
					rw[tmp] = sht.createRow(rwnum++);
					cll[tmp] = rw[tmp].createCell(colnum);
				}
				continue;
			}
			int dishIdx = rwnum-4;
			cll[0] = rw[0].createCell(0);
			cll[0].setCellValue(dList.get(listnum).getName());
			cll[0].setCellStyle(st[3]);
			cll[0] = rw[0].createCell(1);
			cll[0].setCellValue(getMat1(dList.get(listnum).getName()));
			cll[0].setCellStyle(st[3]);
			cll[0] = rw[0].createCell(2);
			cll[0].setCellValue(getMat2(dList.get(listnum).getName()));
			cll[0].setCellStyle(st[3]);
			colnum = 6;
			if(dList.get(listnum).isDrink()){
				for(int i = 6; i < 10; i ++){
					int tasteIdx = rwnum-4;
					for(int sz = 1; sz >= 0; sz--){
						int count = 0;
						for(int j = 0; j < dList.get(listnum).getSize(); j++){
							if(dList.get(listnum).getItemOnList(j).getListItemTas()==i &&
									dList.get(listnum).getItemOnList(j).getListItemSize()==sz){
								cll[0] = (rw[0].getCell(3)==null)?rw[0].createCell(3):rw[0].getCell(3);//rw[0].createCell(3);
								cll[0].setCellValue(dList.get(listnum).getItemOnList(j).getListItemTasName());
								cll[0].setCellStyle(st[3]);
								/*for(int k = 0; k < 5; k++)
									sht.addMergedRegion(new CellRangeAddress(cll[0].getRowIndex(),cll[0].getRowIndex()+3,cll[0].getColumnIndex()+1-k,cll[0].getColumnIndex()+1-k));*/
								for(int tmp = 0; tmp < 4; tmp++)
									cll[tmp] = rw[tmp].createCell(colnum);
								markName(cll, dList.get(listnum).getItemOnList(j).getListItemName()
										,dList.get(listnum).getItemOnList(j).getListItemStat()
										,dList.get(listnum).getItemOnList(j).getListItemCmt(),"", st);
								colnum++;
								count++;
							}
							if(colnum == 16){
								cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
								cll[0].setCellValue(count);
								cll[0].setCellStyle(st[3]);
								count = 0;
								colnum = 6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
						if(colnum > 6){
							cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
							cll[0].setCellValue(count);
							cll[0].setCellStyle(st[3]);
							colnum = 6;
							for(int tmp = 0; tmp < 4; tmp++){
								rw[tmp] = sht.createRow(rwnum++);
								cll[tmp] = rw[tmp].createCell(colnum);
							}
							RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
							RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
							RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
							RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
						}
					}
					if(rwnum-4 > tasteIdx)
						sht.addMergedRegion(new CellRangeAddress(tasteIdx,rwnum-5,3,3));
					
				}
			}else{
				for(int i = 0; i < 6; i ++){
					boolean flg_newline = true;
					int tmpcnt = 0;
					for(int j = 0; j < dList.get(listnum).getSize(); j++)
						if(dList.get(listnum).getItemOnList(j).getListItemTas() == i)
							tmpcnt++;
					if(tmpcnt < 11) flg_newline = false;
					int tasteIdx = rwnum-4;
					for(int sz = 1; sz >= 0; sz--){
						if(sz == 0) flg_newline = true;
						int count = 0;
						for(int j = 0; j < dList.get(listnum).getSize(); j++){
							if(dList.get(listnum).getItemOnList(j).getListItemTas()==i &&
									dList.get(listnum).getItemOnList(j).getListItemSize()==sz){
								cll[0] = (rw[0].getCell(3)==null)?rw[0].createCell(3):rw[0].getCell(3);//rw[0].createCell(3);
								cll[0].setCellValue(dList.get(listnum).getItemOnList(j).getListItemTasName());
								cll[0].setCellStyle(st[3]);
								/*for(int k = 0; k < 5; k++)
									sht.addMergedRegion(new CellRangeAddress(cll[0].getRowIndex(),cll[0].getRowIndex()+3,cll[0].getColumnIndex()+1-k,cll[0].getColumnIndex()+1-k));*/
								for(int tmp = 0; tmp < 4; tmp++)
									cll[tmp] = rw[tmp].createCell(colnum);
								markName(cll, dList.get(listnum).getItemOnList(j).getListItemName()
										,dList.get(listnum).getItemOnList(j).getListItemStat()
										,dList.get(listnum).getItemOnList(j).getListItemCmt(),"", st);
								colnum++;
								count++;
							}
							if(colnum == 16){
								cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
								if(count > 0)
									cll[0].setCellValue(cll[0].getStringCellValue() + String.valueOf(count) + ((sz==0)?"半":"全"));
								cll[0].setCellStyle(st[3]);
								count = 0;
								colnum = 6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
						if(colnum > 6){
							cll[0] = (rw[0].getCell(4)==null)?rw[0].createCell(4):rw[0].getCell(4);//rw[0].createCell(4);
							if(count > 0)
								cll[0].setCellValue(cll[0].getStringCellValue() + String.valueOf(count) + ((sz==0)?"半":"全"));
							cll[0].setCellStyle(st[3]);
							if(flg_newline){
								colnum = 6;
								for(int tmp = 0; tmp < 4; tmp++){
									rw[tmp] = sht.createRow(rwnum++);
									cll[tmp] = rw[tmp].createCell(colnum);
								}
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-5,rwnum-5,6,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-5,rwnum-5,0,15), sht, wb);
								RegionUtil.setBorderTop(1, new CellRangeAddress(rwnum-4,rwnum-4,0,15), sht, wb);
								RegionUtil.setBorderBottom(1, new CellRangeAddress(rwnum-6,rwnum-6,6,15), sht, wb);
							}
						}
					}
					if(rwnum-4 > tasteIdx)
						sht.addMergedRegion(new CellRangeAddress(tasteIdx,rwnum-5,3,3));
				}
			}
			
			for(int i = 0; i < 3; i++)
				sht.addMergedRegion(new CellRangeAddress(dishIdx,rwnum-5,i,i));
			
		}
		for(int tmpcol = 0; tmpcol<16; tmpcol++){
			RegionUtil.setBorderLeft(1, new CellRangeAddress(tmprow,rwnum-5,tmpcol,tmpcol), sht, wb);
			RegionUtil.setBorderRight(1, new CellRangeAddress(tmprow,rwnum-5,tmpcol,tmpcol), sht, wb);
		}
		
		for(int i = tmprow; i < rwnum; i+=4)
			sht.addMergedRegion(new CellRangeAddress(i,i+3,4,4));
		
		
		return wb;
	}
	
	public XSSFWorkbook writeOrder(XSSFWorkbook wb){
		XSSFSheet sht = wb.createSheet("順序");
		Row rw = sht.createRow(0);
		Cell cll = rw.createCell(0);
		cll.setCellValue("中午出餐");
		cll = rw.createCell(1);
		cll.setCellValue("晚上出餐");
		cll = rw.createCell(2);
		cll.setCellValue("菜名");
		cll = rw.createCell(3);
		cll.setCellValue("材料");
		cll = rw.createCell(4);
		cll.setCellValue("材料");
		
		int lsize = lList.size() - 2; // deducting markT and tomato
		int dsize = dList.size() - 2;
		int dishNum = dishMat.size(); // reasonably assume dishNum >= lsize and dsize
		int rwnum = 1;
		
		if(lsize>dsize){
			for(int i = 0; i < dsize; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue(lList.get(i).getName());
				cll = rw.createCell(1);
				cll.setCellValue(dList.get(i).getName());
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
			for(int i = dsize; i < lsize; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue(lList.get(i).getName());
				cll = rw.createCell(1);
				cll.setCellValue("");
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
			for(int i = lsize; i < dishNum; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue("");
				cll = rw.createCell(1);
				cll.setCellValue("");
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
		}else{
			for(int i = 0; i < lsize; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue(lList.get(i).getName());
				cll = rw.createCell(1);
				cll.setCellValue(dList.get(i).getName());
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
			for(int i = lsize; i < dsize; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue("");
				cll = rw.createCell(1);
				cll.setCellValue(dList.get(i).getName());
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
			for(int i = dsize; i < dishNum; i++){
				rw = sht.createRow(rwnum++);
				cll = rw.createCell(0);
				cll.setCellValue("");
				cll = rw.createCell(1);
				cll.setCellValue("");
				cll = rw.createCell(2);
				cll.setCellValue(dishMat.get(i).getDishName());
				cll = rw.createCell(3);
				cll.setCellValue(dishMat.get(i).getMat1());
				cll = rw.createCell(4);
				cll.setCellValue(dishMat.get(i).getMat2());
			}
		}
		
		return wb;
	}
	
	public void readOrder(XSSFSheet sht){
		int rwnum = 1;
		Row rw = sht.getRow(rwnum++);
		while(rw != null){
			if(rw.getCell(0) != null)
				for(int i = 0; i < lList.size(); i++)
					if(lList.get(i).getName().equals(rw.getCell(0).toString())){
						dishList tmp = lList.get(i);
						lList.remove(i);
						lList.add(rwnum-2, tmp);
					}
			if(rw.getCell(1) != null)
				for(int i = 0; i < dList.size(); i++)
					if(dList.get(i).getName().equals(rw.getCell(1).toString())){
						dishList tmp = dList.get(i);
						dList.remove(i);
						dList.add(rwnum-2, tmp);
					}
			if(rw.getCell(2) != null)
				dishMat.add(new dish(rw.getCell(2).toString(), rw.getCell(3).toString(), rw.getCell(4).toString()));
			rw = sht.getRow(rwnum++);
		}
		sortList();
		mapMat();
	}
	
	public void markName(Cell[] cll, String s1, String s2, String s3, String s4, CellStyle[] st){
		int idx = s2.indexOf(",");
		if(idx > -1 && s3.equals("")){
			s3 = s2.substring(idx+1, s2.length());
			s2 = s2.substring(0, idx);
		}
		idx = s3.indexOf(",");
		if(idx > -1 && s4.equals("")){
			s4 = s3.substring(idx+1, s3.length());
			s3 = s3.substring(0, idx);
		}
		cll[0].setCellValue(s1);
		cll[0].setCellStyle(st[0]);
		cll[1].setCellValue(s2);
		cll[1].setCellStyle(st[4]);
		cll[2].setCellValue(s3);
		cll[2].setCellStyle(st[4]);
		cll[3].setCellValue(s4);
		cll[3].setCellStyle(st[1]);
		}
	
	
	
	@SuppressWarnings("deprecation")
	public XSSFSheet markT(XSSFSheet shet, int nowrw, String str, CellStyle[] st, boolean flg){
		Row rw; Cell cll;
		if(flg){
			rw = shet.createRow(nowrw++);
			shet.addMergedRegion(new CellRangeAddress(nowrw-1, nowrw-1, 0, 2));
			shet.addMergedRegion(new CellRangeAddress(nowrw-1, nowrw-1, 6, 9));
			cll = rw.createCell(6);
			cll.setCellValue(str);
			cll.setCellStyle(st[2]);
		}
		rw = shet.createRow(nowrw);
		int col = 0;
		cll = rw.createCell(col++);
		cll.setCellValue("名稱");
		cll.setCellStyle(st[2]);
		cll = rw.createCell(col);
		cll.setCellValue("材料");
		shet.addMergedRegion(new CellRangeAddress(nowrw,nowrw,1,2));
		cll.setCellStyle(st[2]);
		col+=2;
		cll = rw.createCell(col++);
		cll.setCellValue("味道");
		cll.setCellStyle(st[2]);
		cll = rw.createCell(col++);
		cll.setCellValue("份數");
		cll.setCellStyle(st[2]);
		cll = rw.createCell(col++);
		cll.setCellValue("特殊註記");
		cll.setCellStyle(st[2]);
		for(int i = 1; i < 11; i++){
			cll = rw.createCell(col++);
			cll.setCellValue(i);
			cll.setCellStyle(st[2]);
		}
		
		return shet;
	}
	
	public void checkDish(String dnm, String mat1, String mat2){
		boolean flg = true;
		for(int i = 0; i < dishMat.size(); i++)
			if(dishMat.get(i).getDishName().equals(dnm)){
				dishMat.get(i).setMat((mat1==null)?"":mat1, (mat2==null)?"":mat2);
				flg = false;
			}
		if(flg)
			dishMat.add(new dish(dnm, (mat1==null)?"":mat1, (mat2==null)?"":mat2));
	}
	
	public String getMat1(String dnm){
		for(int i = 0; i < dishMat.size(); i++)
			if(dnm.equals(dishMat.get(i).getDishName()))
				return dishMat.get(i).getMat1();
		return "";
	}
	
	public String getMat2(String dnm){
		for(int i = 0; i < dishMat.size(); i++)
			if(dnm.equals(dishMat.get(i).getDishName()))
				return dishMat.get(i).getMat2();
		return "";
	}
	
	@SuppressWarnings("deprecation")
	public XSSFWorkbook writePrepare(XSSFWorkbook wb, boolean flg){
		XSSFSheet sht = wb.createSheet("備料");
		CellStyle stl = wb.createCellStyle();
		stl.setBorderBottom(CellStyle.BORDER_THIN);
		stl.setBorderTop(CellStyle.BORDER_THIN);
		stl.setBorderLeft(CellStyle.BORDER_THIN);
		stl.setBorderRight(CellStyle.BORDER_THIN);
		stl.setWrapText(true);
		stl.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		stl.setAlignment(CellStyle.ALIGN_CENTER);
		XSSFFont ft = wb.createFont();
		ft.setFontHeightInPoints((short)12);
		ft.setFontName("新細明體");
		stl.setFont(ft);
		sht.setColumnWidth(0, 4450);
		sht.setColumnWidth(1, 6000);
		sht.setColumnWidth(2, 9500);
		for(int i = 3; i < 5; i++)
			sht.setColumnWidth(i, 3100);
		Row rw = sht.createRow(0);
		rw.setHeightInPoints((float) 16.5);
		rw.createCell(0).setCellValue("月 日 星期");
		sht.addMergedRegion(new CellRangeAddress(0,0,0,2));
		rw = sht.createRow(1);
		rw.setHeightInPoints(33);
		rw.createCell(0).setCellValue("名稱");
		rw.getCell(0).setCellStyle(stl);
		rw.createCell(1).setCellValue("材料");
		sht.addMergedRegion(new CellRangeAddress(1,1,1,2));
		rw.getCell(1).setCellStyle(stl);
		rw.createCell(2).setCellStyle(stl);
		rw.createCell(3).setCellValue("總份數");
		rw.createCell(4).setCellValue("晚餐\n份數");
		for(int i = 3; i < 5; i++)
			rw.getCell(i).setCellStyle(stl);
		
		int rwnum = 2;
		
		if(flg)
			for(int i = 0; i < pList.size(); i++){
				if(pList.get(i).getName().equals("======分隔標題======")) continue;
				rw = sht.createRow(rwnum++);
				rw.setHeightInPoints(66);
				rw.createCell(0).setCellValue(pList.get(i).getName());
				rw.createCell(1).setCellValue(pList.get(i).getMat1());
				rw.createCell(2).setCellValue(pList.get(i).getMat2());
				String tmp = "";
				if(pList.get(i).getDF()+pList.get(i).getLF() != 0)
					tmp = tmp + String.valueOf(Math.abs(pList.get(i).getDF()+pList.get(i).getLF())) + "全";
				if(pList.get(i).getDH()+pList.get(i).getLH() != 0)
					tmp = tmp + String.valueOf(Math.abs(pList.get(i).getDH()+pList.get(i).getLH())) + "半";
				rw.createCell(3).setCellValue(tmp);
				tmp = "";
				if(pList.get(i).getDF() != 0)
					tmp = tmp + String.valueOf(pList.get(i).getDF()) + "全";
				if(pList.get(i).getDH() != 0)
					tmp = tmp + String.valueOf(pList.get(i).getDH()) + "半";
				rw.createCell(4).setCellValue(tmp);
			}
		
		for(int i = 2; i < rwnum; i++)
			for(int j = 0; j < 5; j++)
				sht.getRow(i).getCell(j).setCellStyle(stl);
		
		return wb;
	}
	
	public void countPrepare(boolean flg){
		nList.clear();
		boolean idx;
		for(int i = 0; i < lList.size(); i++){
			idx = true;
			for(int j = 0; j < nList.size(); j++)
				if(nList.get(j).getName().equals(lList.get(i).getName())){
					idx = false;
					for(int k = 0; k < lList.get(i).getSize(); k++)
						if(!lList.get(i).getItemOnList(k).getListItemStat().equals("晚上出")){
							if(lList.get(i).getItemOnList(k).getListItemSize() == 1)
								nList.get(j).fPlusL();
							else
								nList.get(j).hPlusL();
						}
				}
			if(idx){
				nList.add(new prepareItem(lList.get(i).getName(), lList.get(i).getCmt1(), lList.get(i).getCmt2()));
				for(int k = 0; k < lList.get(i).getSize(); k++)
					if(!lList.get(i).getItemOnList(k).getListItemStat().equals("晚上出")){
						if(lList.get(i).getItemOnList(k).getListItemSize() == 1)
							nList.get(nList.size()-1).fPlusL();
						else
							nList.get(nList.size()-1).hPlusL();
					}
			}
		}
		for(int i = 0; i < dList.size(); i++){
			idx = true;
			for(int j = 0; j < nList.size(); j++)
				if(nList.get(j).getName().equals(dList.get(i).getName())){
					idx = false;
					for(int k = 0; k < dList.get(i).getSize(); k++)
						if(!dList.get(i).getItemOnList(k).getListItemStat().equals("晚上出")){
							if(dList.get(i).getItemOnList(k).getListItemSize() == 1)
								nList.get(j).fPlusD();
							else
								nList.get(j).hPlusD();
						}
				}
			if(idx){
				nList.add(new prepareItem(dList.get(i).getName(), dList.get(i).getCmt1(), dList.get(i).getCmt2()));
				for(int k = 0; k < dList.get(i).getSize(); k++)
					if(!dList.get(i).getItemOnList(k).getListItemStat().equals("晚上出")){
						if(dList.get(i).getItemOnList(k).getListItemSize() == 1)
							nList.get(nList.size()-1).fPlusD();
						else
							nList.get(nList.size()-1).hPlusD();
					}
			}
		}
		
		if(flg)
			pList.addAll(nList);
	}
	
	public void clearPrepare(){
		pList.clear();
	}
	
	@SuppressWarnings("deprecation")
	public XSSFWorkbook writeAddMinus(XSSFWorkbook wb){
		XSSFSheet sht = wb.createSheet("加退料");
		CellStyle stl = wb.createCellStyle();
		stl.setBorderBottom(CellStyle.BORDER_THIN);
		stl.setBorderTop(CellStyle.BORDER_THIN);
		stl.setBorderLeft(CellStyle.BORDER_THIN);
		stl.setBorderRight(CellStyle.BORDER_THIN);
		stl.setWrapText(true);
		stl.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		stl.setAlignment(CellStyle.ALIGN_CENTER);
		XSSFFont ft = wb.createFont();
		ft.setFontHeightInPoints((short)12);
		ft.setFontName("新細明體");
		stl.setFont(ft);
		sht.setColumnWidth(0, 3500);
		sht.setColumnWidth(1, 5000);
		sht.setColumnWidth(2, 7500);
		for(int i = 3; i < 7; i++)
			sht.setColumnWidth(i, 2500);
		Row rw = sht.createRow(0);
		rw.setHeightInPoints((float) 16.5);
		for(int i = 0; i < 7; i++)
			rw.createCell(i).setCellStyle(stl);
		rw.getCell(0).setCellValue("月 日   加料 & 退料單");
		rw.getCell(3).setCellValue("午");
		rw.getCell(5).setCellValue("晚");
		rw = sht.createRow(1);
		rw.setHeightInPoints((float)16.5);
		for(int i = 0; i < 7; i++)
			rw.createCell(i).setCellStyle(stl);
		rw.getCell(3).setCellValue("加料");
		rw.getCell(4).setCellValue("退料");
		rw.getCell(5).setCellValue("加料");
		rw.getCell(6).setCellValue("退料");
		sht.addMergedRegion(new CellRangeAddress(0,1,0,2));
		sht.addMergedRegion(new CellRangeAddress(0,0,3,4));
		sht.addMergedRegion(new CellRangeAddress(0,0,5,6));
		
		countPrepare(false);
		mapMat();
		
		int rwnum = 2;
		for(int i = 0; i < nList.size(); i++)
			for(int j = 0; j < pList.size(); j++)
				if(nList.get(i).getName().equals(pList.get(j).getName())){
					if(nList.get(i).getDF() != pList.get(j).getDF() || nList.get(i).getDH() != pList.get(j).getDH() 
							|| nList.get(i).getLF() != pList.get(j).getLF() || nList.get(i).getLH() != pList.get(j).getLH()){
						rw = sht.createRow(rwnum++);
						rw.setHeightInPoints(66);
						rw.createCell(0).setCellValue(nList.get(i).getName());
						rw.createCell(1).setCellValue(nList.get(i).getMat1());
						rw.createCell(2).setCellValue(nList.get(i).getMat2());
						int df = nList.get(i).getDF() - pList.get(j).getDF();
						int dh = nList.get(i).getDH() - pList.get(j).getDH();
						int lf = nList.get(i).getLF() - pList.get(j).getLF();
						int lh = nList.get(i).getLH() - pList.get(j).getLH();
						
						String[] tmp = countAddMinus(lf, lh);
						rw.createCell(3).setCellValue(tmp[0]);
						rw.createCell(4).setCellValue(tmp[1]);
						tmp = countAddMinus(df, dh);
						rw.createCell(5).setCellValue(tmp[0]);
						rw.createCell(6).setCellValue(tmp[1]);
					}
				}
		for(int i = 0; i < pList.size(); i++){
			boolean flg = true;
			for(int j = 0; j < nList.size(); j++){
				if(pList.get(i).getName().equals(nList.get(j).getName())){
					flg = false; continue;
				}
			}
			if(flg){
				rw = sht.createRow(rwnum++);
				rw.setHeightInPoints(66);
				rw.createCell(0).setCellValue(pList.get(i).getName());
				rw.createCell(1).setCellValue(pList.get(i).getMat1());
				rw.createCell(2).setCellValue(pList.get(i).getMat2());
				String[] tmp = countAddMinus(-1*pList.get(i).getLF(), -1*pList.get(i).getLH());
				rw.createCell(3).setCellValue(tmp[0]);
				rw.createCell(4).setCellValue(tmp[1]);
				tmp = countAddMinus(-1*pList.get(i).getDF(), -1*pList.get(i).getDH());
				rw.createCell(5).setCellValue(tmp[0]);
				rw.createCell(6).setCellValue(tmp[1]);
			}
		}
		for(int i = 0; i < nList.size(); i++){
			boolean flg = true;
			for(int j = 0; j < pList.size(); j++){
				if(nList.get(i).getName().equals(pList.get(j).getName())||nList.get(i).getName().equals("======分隔標題======")){
					flg = false; continue;
				}
			}
			if(flg){
				rw = sht.createRow(rwnum++);
				rw.setHeightInPoints(66);
				rw.createCell(0).setCellValue(nList.get(i).getName());
				rw.createCell(1).setCellValue(nList.get(i).getMat1());
				rw.createCell(2).setCellValue(nList.get(i).getMat2());
				String[] tmp = countAddMinus(nList.get(i).getLF(), nList.get(i).getLH());
				rw.createCell(3).setCellValue(tmp[0]);
				rw.createCell(4).setCellValue(tmp[1]);
				tmp = countAddMinus(nList.get(i).getDF(), nList.get(i).getDH());
				rw.createCell(5).setCellValue(tmp[0]);
				rw.createCell(6).setCellValue(tmp[1]);
			}
		}
		
		for(int i = 2; i < rwnum; i++)
			for(int j = 0; j < 7; j++)
				sht.getRow(i).getCell(j).setCellStyle(stl);
		
		return wb;
	}
	
	public String[] countAddMinus(int full, int half){
		String[] tmp = {"",""};
		
		if(full > 0)
			tmp[0] = tmp[0] + String.valueOf(full) + "全";
		else if(full < 0)
			tmp[1] = tmp[1] + String.valueOf(-1*full) + "全";
		
		if(half > 0)
			tmp[0] = tmp[0] + String.valueOf(half) + "半";
		else if(half < 0)
			tmp[1] = tmp[1] + String.valueOf(-1*half) + "半";
		
		return tmp;
	}
	
	public void mapMat(){
		for(int i = 0; i < pList.size(); i++)
			for(int j = 0; j < dishMat.size(); j++)
				if(pList.get(i).getName().equals(dishMat.get(j).getDishName()))
					pList.get(i).setMat(dishMat.get(j).getMat1(), dishMat.get(j).getMat2());
		for(int i = 0; i < nList.size(); i++)
			for(int j = 0; j < dishMat.size(); j++)
				if(nList.get(i).getName().equals(dishMat.get(j).getDishName()))
					nList.get(i).setMat(dishMat.get(j).getMat1(), dishMat.get(j).getMat2());
	}
	
	public void updateMod(JLabel modName, JLabel origDishName, JTextField newDishName){
		if(dfList.size()>0){
			diffDish now = dfList.get(0);
			
			modName.setText(now.getPName());
			origDishName.setText(now.getOName());
			newDishName.setText(now.getNName());
		}else{
			modName.setText("病患名");
			origDishName.setText("原菜名");
			newDishName.setText("新菜名");
			String omsg = "沒有新的異動了";
			JOptionPane.showMessageDialog(null, omsg, "Update done", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void updateTotalDish(JComboBox<String> jcombobox){
		String[] tmp = new String[dishMat.size()];
		for(int i = 0; i < dishMat.size(); i++)
			tmp[i] = dishMat.get(i).getDishName();
		jcombobox.setModel(new DefaultComboBoxModel<String>(tmp));
	}
}
