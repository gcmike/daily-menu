import java.util.*;

public class dishList {
	private ArrayList<dishListItem> namelist = new ArrayList<dishListItem>();
	private String dName;
	private String Cmt1;
	private String Cmt2;
	private boolean drink;
	
	public dishList(String nm, String c1, String c2, int ml){dName = nm; Cmt1 = c1; Cmt2 = c2; drink = (ml==1||ml==2||ml==6||ml==7)?true:false;}
	public dishList(String nm, String c1, String c2, patient pnm, int ml){
		dName = nm; Cmt1 = c1; Cmt2 = c2;
		namelist.add(new dishListItem(nm, pnm,ml));
		drink = (ml==1||ml==2||ml==6||ml==7)?true:false;
		}
	public String getName(){return dName;}
	public String getCmt1(){return Cmt1;}
	public String getCmt2(){return Cmt2;}
	public dishListItem getItemOnList(int idx){return namelist.get(idx);}
	public void removeItemOnList(int idx){namelist.remove(idx);}
	public int getSize(){return namelist.size();}
	public boolean isDrink(){return drink;}
	public void addList(patient nm, int ml){namelist.add(new dishListItem(dName, nm,ml));}
	public void removeList(patient pnm){
		for(int i = namelist.size()-1; i >= 0; i--)
			if(namelist.get(i).getPatient().getName().equals(pnm.getName()))
				namelist.remove(i);
	}
	
}
