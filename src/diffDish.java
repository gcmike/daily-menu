public class diffDish {
	private String pname;
	private int meal;
	private String origName;
	private String newName;
	private String newCmt1;
	private String newCmt2;
	
	diffDish(String pn, int ml, String on, String nn, String c1, String c2)
	{pname = pn; meal = ml; origName = on; newName = nn; newCmt1 = c1; newCmt2 = c2;}
	String getPName(){return pname;}
	int getMeal(){return meal;}
	String getOName(){return origName;}
	String getNName(){return newName;}
	String getCmt1(){return newCmt1;}
	String getCmt2(){return newCmt2;}
}
