
public class prepareItem {
	private String dname;
	private String cmt1;
	private String cmt2;
	private int lhalf;
	private int lfull;
	private int dhalf;
	private int dfull;
	
	public prepareItem(String dnm, String c1, String c2){dname = dnm; cmt1 = c1; cmt2 = c2; lhalf = lfull = dhalf = dfull = 0;}
	public prepareItem(String dnm, String c1, String c2, int lf, int lh, int df, int dh)
	{dname = dnm; cmt1 = c1; cmt2 = c2; lhalf = lh; lfull = lf; dhalf = dh; dfull = df;}
	public void setMat(String c1, String c2){cmt1 = c1; cmt2 = c2;}
	public int getLH(){return lhalf;}
	public int getLF(){return lfull;}
	public int getDH(){return dhalf;}
	public int getDF(){return dfull;}
	public String getName(){return dname;}
	public String getMat1(){return cmt1;}
	public String getMat2(){return cmt2;}
	public void hPlusL(){lhalf++;}
	public void fPlusL(){lfull++;}
	public void hPlusD(){dhalf++;}
	public void fPlusD(){dfull++;}
	public void hPlusL(int num){lhalf += num;}
	public void fPlusL(int num){lfull += num;}
	public void hPlusD(int num){dhalf += num;}
	public void fPlusD(int num){dfull += num;}
}
