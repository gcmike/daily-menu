import java.util.*;

public class patient {
	private String name;
	private ArrayList<dish> lunch = new ArrayList<dish>();
	private ArrayList<dish> dinner = new ArrayList<dish>();
	private ArrayList<dish> bkfast = new ArrayList<dish>();
	private ArrayList<dish> sweet = new ArrayList<dish>();
	private ArrayList<dish> drink = new ArrayList<dish>();
	private boolean tomato;
	private String tomatoCmt;
	private boolean ship;
	private boolean order = true;
	
	public patient(String fnm){name = fnm;}
	public void addLunch(dish newDish){lunch.add(newDish);}
	public void addDinner(dish newDish){dinner.add(newDish);}
	public void addBkfast(dish newDish){bkfast.add(newDish);}
	public void setName(String fnm){name = fnm;}
	public void addSweet(dish newDish){sweet.add(newDish);}
	public void addDrink(dish dk){drink.add(dk);}
	public void setTomato(boolean tmt){tomato = tmt;}
	public void setTomatoCmt(String st){tomatoCmt = st;}
	public void setShip(boolean shp){ship = shp;}
	public void setOrder(boolean st){order = st;}
	
	public boolean getShip(){return ship;}
	public boolean getTomato(){return tomato;}
	public String getTomatoCmt(){return tomatoCmt;}
	public String getTomatoStat(){
		if(ship)
			return tomato?"�p���":"�����X";
		else{
			if(lunch.size()>0){
				if(dinner.size()>0)
					return tomato?"":"�����X";
				else
					return tomato?"�p���":"�����X";
			}else
				return tomato?"�p���":"�����X";
		}
	}
	public String getName(){return name;}
	public dish getSweet(int idx){return sweet.get(idx);}
	public boolean emptySweet(){return sweet.size()>0;}
	public dish getDrink(int idx){return drink.get(idx);}
	public boolean emptyDrink(){return drink.size()>0;}
	public boolean emptyLunch(){return lunch.size()>0;}
	public dish getLunch(int idx){return lunch.get(idx);}
	public int getLunchCount(){return lunch.size();}
	public boolean emptyDinner(){return dinner.size()>0;}
	public dish getDinner(int idx){return dinner.get(idx);}
	public int getDinnerCount(){return dinner.size();}
	public boolean emptyBkfast(){return bkfast.size()>0;}
	public dish getBkfast(int idx){return bkfast.get(idx);}
	public boolean getOrder(){return order;}
	public int getCount(){return bkfast.size()+lunch.size()+dinner.size()+drink.size()+sweet.size()+((ship==true?1:0)+1);}
	
	public void clearSweet(){
		while(sweet.size()>0)
			sweet.remove(0);
	}
	public void clearDrink(){
		while(drink.size()>0)
			drink.remove(0);
	}
	public void clearBkfast(){
		while(bkfast.size()>0)
			bkfast.remove(0);
	}
	public void clearLunch(){
		while(lunch.size()>0)
			lunch.remove(0);
	}
	public void clearDinner(){
		while(dinner.size()>0)
			dinner.remove(0);
	}
}
