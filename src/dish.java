
public class dish {
	private String dishName = "";
	private int tas = 0;
	private boolean sz = false;
	private String cmt = "";
	private String Cmt1 = "";
	private String Cmt2 = "";
	
	public dish(){}
	public dish(String dname){dishName = dname;}
	public dish(String dname, String c1, String c2){dishName = dname; Cmt1 = c1; Cmt2 = c2;}
	public dish(String dname, int ts){dishName = dname; tas = ts;}
	public dish(String dname, int ts, String ct){dishName = dname; tas = ts; cmt = ct;}
	public dish(String dname, int ts, String ct, boolean size){dishName = dname; tas = ts; cmt = ct; sz = size;}
	public String getDishName(){return dishName;}
	public String getMat1(){return Cmt1;}
	public String getMat2(){return Cmt2;}
	public void setName(String nm){dishName = nm;}
	public void setMat(String m1, String m2){Cmt1 = m1; Cmt2 = m2;}
	public int getTas(){return tas;}
	public String getTasName(){
		switch(tas){
		case 0:
			return "不調味";
		case 1:
			return "淡淡";
		case 2:
			return "稍淡";
		case 3:
			return "正常";
		case 4:
			return "稍重";
		case 5:
			return "重";
		case 6:
			return "無糖";
		case 7:
			return "少糖";
		case 8:
			return "正常";
		case 9:
			return "多糖";
		default:
			return "無法";
		}
				
	}
	public boolean getSize(){return sz;}
	public String getComment(){return cmt;}
	public void copyDish(dish in){dishName = in.getDishName(); tas = in.getTas(); cmt = in.getComment(); sz = in.getSize();}
}
