
public class dishListItem {
	/*
	 * 0: tomato
	 * 1: drink
	 * 2: sweet
	 * 3: breakfast
	 * 4: lunch
	 * 5: dinner
	 * 6: drink night
	 * 7: sweet night
	 */
	private int meal;
	private patient pthis;
	private String dishName;
	
	public dishListItem(String dnm, patient pnm, int ml){dishName = dnm; pthis = pnm; meal = ml;}
	public int getMeal(){return meal;}
	public patient getPatient(){return pthis;}
	public String getListItemName(){return pthis.getName();}
	public int getListItemTas(){
		switch(meal){
		case 2:
		case 7:
			return pthis.getSweet(0).getTas();
		case 1:
		case 6:
			return pthis.getDrink(0).getTas();
		case 3:
			return pthis.getBkfast(0).getTas();
		case 4:
			for(int i = 0; i < pthis.getLunchCount(); i++)
				if(pthis.getLunch(i).getDishName().equals(dishName))
					return pthis.getLunch(i).getTas();
		case 5:
			for(int i = 0; i < pthis.getDinnerCount(); i++)
				if(pthis.getDinner(i).getDishName().equals(dishName))
					return pthis.getDinner(i).getTas();
			default:
				return 0;
		}
	}
	public String getListItemTasName(){
		switch(meal){
		case 2:
		case 7:
			return pthis.getSweet(0).getTasName();
		case 1:
		case 6:
			return pthis.getDrink(0).getTasName();
		case 3:
			return pthis.getBkfast(0).getTasName();
		case 4:
			for(int i = 0; i < pthis.getLunchCount(); i++)
				if(pthis.getLunch(i).getDishName().equals(dishName))
					return pthis.getLunch(i).getTasName();
		case 5:
			for(int i = 0; i < pthis.getDinnerCount(); i++)
				if(pthis.getDinner(i).getDishName().equals(dishName))
					return pthis.getDinner(i).getTasName();
			default:
				return "";
		}
	}
	public int getListItemSize(){
		switch(meal){
		case 2:
		case 7:
			return (pthis.getSweet(0).getSize())?1:0;
		case 1:
		case 6:
			return (pthis.getDrink(0).getSize())?1:0;
		case 3:
			return (pthis.getBkfast(0).getSize())?1:0;
		case 4:
			for(int i = 0; i < pthis.getLunchCount(); i++)
				if(pthis.getLunch(i).getDishName().equals(dishName))
					return (pthis.getLunch(i).getSize()?1:0);
		case 5:
			for(int i = 0; i < pthis.getDinnerCount(); i++)
				if(pthis.getDinner(i).getDishName().equals(dishName))
					return (pthis.getDinner(i).getSize())?1:0;
			default:
				return 0;
		}
	}
	public String getListItemStat(){
		switch(meal){
		case 0:
			return "";
		case 1:
			return "";
		case 2:
			return "";
		case 3:
			return pthis.getBkfast(0).getSize()?"���\��":"���\�b";
		case 4:
			for(int i = 0; i < pthis.getLunchCount(); i++)
				if(pthis.getLunch(i).getDishName().equals(dishName))
					return pthis.getLunch(i).getSize()?"���\��":"���\�b";
		case 5:
			for(int i = 0; i < pthis.getDinnerCount(); i++)
				if(pthis.getDinner(i).getDishName().equals(dishName))
					return pthis.getDinner(i).getSize()?"���\��":"���\�b";
		case 6:
			return "�ߤW�X";
		case 7:
			return "�ߤW�X";
			default:
				return "ERROR";
		}
	}
	public String getListItemCmt(){
		switch(meal){
		case 0:
			return pthis.getTomatoStat();
		case 1:
			return pthis.getDrink(0).getComment();
		case 2:
			return pthis.getSweet(0).getComment();
		case 3:
			return pthis.getBkfast(0).getComment();
		case 4:
			for(int i = 0; i < pthis.getLunchCount(); i++)
				if(pthis.getLunch(i).getDishName().equals(dishName))
					return pthis.getLunch(i).getComment();
		case 5:
			for(int i = 0; i < pthis.getDinnerCount(); i++)
				if(pthis.getDinner(i).getDishName().equals(dishName))
					return pthis.getDinner(i).getComment();
		case 6:
			return "";
		case 7:
			return "";
			default:
				return "ERROR";
		}
	}
}
